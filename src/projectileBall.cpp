#include <iostream>
#include <fstream>
using namespace std;
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "model3d.h"
#include "model3d_cooktorrance.h"
#include "projectileBall.h"
#include "collisionDetection.h"
#include "lightData.h"
#include "renderStruct.h"
#include "texture.h"

//#define DEBUG

Model3d* ProjectileBall::m_model = NULL;
Model3dCookTorrance* ProjectileBall::m_modelCookTorrance = NULL;

//----------------------------------------------------------------------
void ProjectileBall::InitProjectileBall()
{
	LoadModel();
}
//----------------------------------------------------------------------
void ProjectileBall::LoadModel()
{
	m_model = new Model3d;
	m_model->Build("ball.obj");
	m_model->SetTexture(GetTextureResource(PROJECTILE_BALL_ALBEDO_TEXTURE));

	m_modelCookTorrance = new Model3dCookTorrance;
	m_modelCookTorrance->Build("ball.obj");
	m_modelCookTorrance->SetTexture(GetTextureResource(PROJECTILE_BALL_ALBEDO_TEXTURE), GetTextureResource(PROJECTILE_BALL_NORMAL_TEXTURE),
									GetTextureResource(PROJECTILE_BALL_METALLIC_TEXTURE), GetTextureResource(PROJECTILE_BALL_ROUGHNESS_TEXTURE),
									GetTextureResource(PROJECTILE_BALL_AO_TEXTURE));
}
//----------------------------------------------------------------------
ProjectileBall::ProjectileBall()
{
	m_moveDir = glm::vec3(0.0f, 0.0f, 0.0f);
		
	m_pos = glm::vec3(0.0f, 0.0f, 0.0f);
	m_rot = glm::vec3(0.0f, 0.0f, 0.0f);
	m_scale = glm::vec3(1.0f, 1.0f, 1.0f);
	m_moveSpeed = 0.0f;

	m_boundingSphereRadius = 1.0f;

	m_destroy = false;
}
//----------------------------------------------------------------------
ProjectileBall::~ProjectileBall()
{
}
//----------------------------------------------------------------------
void ProjectileBall::SetPos(glm::vec3 pos)
{
	m_pos = pos;
}
//----------------------------------------------------------------------
void ProjectileBall::SetScale(float scale)
{
	m_scale = glm::vec3(scale, scale, scale);
	m_boundingSphereRadius = 1.0f * scale;
}
//----------------------------------------------------------------------
void ProjectileBall::SetMoveDir(glm::vec3 dir)
{
	m_moveDir = glm::normalize(dir);

	/*
	// Auto rotate around y axis to face direction we move in
	const glm::vec3 defaultFacingDir(0.0f, -1.0f, 0.0f);
	m_rot.y = acos(glm::dot(defaultFacingDir, m_moveDir)) * 180.0 / M_PI;

	if(dir.x < 0.0f)
	{
		m_rot.y = 360.0f - m_rot.y;
	}
	*/
}
//----------------------------------------------------------------------
void ProjectileBall::SetMoveSpeed(float speed)
{
	m_moveSpeed = speed;
}
//----------------------------------------------------------------------
glm::vec3& ProjectileBall::GetPos()
{
	return m_pos;
}
//----------------------------------------------------------------------
glm::vec3&	ProjectileBall::GetMoveDir()
{
	return m_moveDir;
}
//----------------------------------------------------------------------
float& ProjectileBall::GetBoundingSphereRadius()
{
	return m_boundingSphereRadius;
}
//----------------------------------------------------------------------
bool ProjectileBall::GetDestroyed() const
{
	return m_destroy || m_moveSpeed <= 0.00001f;
}
//----------------------------------------------------------------------
void ProjectileBall::Destroy()
{
	m_destroy = true;
}
//----------------------------------------------------------------------
float& ProjectileBall::GetMoveSpeed()
{
	return m_moveSpeed;
}
//----------------------------------------------------------------------
void ProjectileBall::Tick(float deltaTime)
{
	DoMove(deltaTime, m_pos);
}
//----------------------------------------------------------------------
void ProjectileBall::DoMove(float deltaTime, glm::vec3 &pos)
{
	float moveSpeedTimeAdjusted = m_moveSpeed * deltaTime;
	pos += m_moveDir * moveSpeedTimeAdjusted;

	// Lose speeed gradually
	const float speedFallofFactor = 1.0f;
	m_moveSpeed = m_moveSpeed - speedFallofFactor * deltaTime;
	if(m_moveSpeed < 0.0f)
	{
		m_moveSpeed = 0.0f;
	}
}
//----------------------------------------------------------------------
void ProjectileBall::Render(const RenderStateModel3D& renderState, const glm::mat4 &projection, const glm::mat4 &view)
{
	#ifdef USE_PBR
		m_modelCookTorrance->Render(renderState, projection, view, m_pos, m_rot, m_scale);
	#else
		m_model->Render(renderState, projection, view, m_pos, m_rot, m_scale);
	#endif	
}