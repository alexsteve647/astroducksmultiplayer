#include <iostream>
#include <fstream>
using namespace std;
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "gui.h"
#include "texturedQuad.h"
#include "text2D.h"

//#define DEBUG

//----------------------------------------------------------------------
HUDGUI::HUDGUI()
{
}
//----------------------------------------------------------------------
HUDGUI::~HUDGUI()
{
}
//----------------------------------------------------------------------
void HUDGUI::AddTexturedQuad(TexturedQuad* textureQuad)
{
	m_textureQuads.push_back(textureQuad);
}
//----------------------------------------------------------------------
void HUDGUI::AddText(Text2D* text)
{
	m_texts.push_back(text);
}
//----------------------------------------------------------------------
void HUDGUI::Clear()
{
	m_textureQuads.clear();
	m_texts.clear();
}
//----------------------------------------------------------------------
void HUDGUI::Tick()
{
	for(auto textureQuad : m_textureQuads)
	{
		textureQuad->Tick();
	}
}
//----------------------------------------------------------------------
void HUDGUI::Render(const glm::mat4 &projection, const glm::mat4 &view)
{
	for(auto textureQuad : m_textureQuads)
	{
		textureQuad->Render(projection, view);
	}

	for(auto text : m_texts)
	{		
		text->Render(projection, view);
	}
}
//----------------------------------------------------------------------

