#ifndef IN_GAME_HUD_H
#define IN_GAME_HUD_H

#include <vector>

// Forward declarations
class TexturedQuad;
class HUDGUI;
class Text2D;

class InGameHUD
{
	public:
	InGameHUD(bool singlePlayer);
	~InGameHUD();

	HUDGUI* GetGameHud() const;
	void	UpdatePlayerHealth(int playerHealth);
	void	UpdatePlayerScore(int playerScore);

	void	UpdatePlayerTwoHealth(int playerHealth);
	void	UpdatePlayerTwoScore(int playerScore);
	
	void	UpdateTimeLeft(int minutes, int seconds);
	void	SetMainLoopTime(float v);

	void	PrepareHUDGUIForRender();

	void	ShowGameOverSign(bool b);
	void	ShowTimeMainLoopTime(bool b);

	void 	DebugAddTextureQad(TexturedQuad* theQuad);

	private:

	void	UpdateText(Text2D* text, int value, int length);
	
	HUDGUI* m_gameHUD;

	int		m_playerHealth;
	int		m_playerScore;

	int		m_playerTwoHealth;
	int		m_playerTwoScore;

	int		m_timeLeftMinutes;
	int		m_timeLeftSeconds;

	bool	m_showGameOverSign;
	bool	m_showTimeMainLoopTime;

	std::vector<TexturedQuad*> 	m_HUDElements;
	std::vector<TexturedQuad*> 	m_HUDElementsPlayerTwo;

	Text2D*						m_playerScoreText;
	Text2D*						m_playerTwoScoreText;

	Text2D*						m_TimeLeftLabel;
	Text2D*						m_timeLeftMinutesText;
	TexturedQuad*				m_timeLeftColonSeparator;
	Text2D*						m_timeLeftSecondsText;

	Text2D*						m_timeMainLoopTime;

	Text2D*						m_gameOverSign;
	Text2D*						m_restartGameSign;

	TexturedQuad* 				m_debugTexturedQuad;
};

#endif