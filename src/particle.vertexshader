#version 330 core

layout(location = 0) in vec3 quadVerts;
layout(location = 1) in vec4 posAndSize; // Center position and size of this particle
layout(location = 2) in vec4 color;

out vec4 particlecolor;

// Values that stay constant for the whole mesh.
uniform vec3 CameraRight_worldspace;
uniform vec3 CameraUp_worldspace;
uniform mat4 VP; // View projection, no model matrix needed - positional data comes from posAndSize

void main()
{
	float size = posAndSize.w;
	vec3 center_worldspace = posAndSize.xyz;
	
	vec3 vertexPos_worldspace = center_worldspace + CameraRight_worldspace * quadVerts.x * size
								+ CameraUp_worldspace * quadVerts.y * size;

	gl_Position = VP * vec4(vertexPos_worldspace, 1.0f);
	particlecolor = color;
}