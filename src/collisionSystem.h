#ifndef COLLISION_SYSTEM_H
#define COLLISION_SYSTEM_H

#include <memory>
#include <vector>
#include "quadTree.h"

class Duck;
class Player;
class GameDefData;

class CollisionResult
{
	public:
	CollisionResult() {m_increaseWaterLevel = 0.0f; m_decreaseWaterLevel = 0.0f;}
	~CollisionResult() {}

	float m_increaseWaterLevel;
	float m_decreaseWaterLevel;
};

class CollisionEngine
{
	public:
	~CollisionEngine();

	static CollisionEngine& GetInstance(std::vector<Duck* > &ducks, std::vector<std::unique_ptr<Player> > &players);

	void Tick(const GameDefData* gameDefData, CollisionResult &result);

	private:

	CollisionEngine(std::vector<Duck* > &ducks, std::vector<std::unique_ptr<Player> > &players);		

	void HandleProjectileCollision(const GameDefData* gameDefData, CollisionResult &result) ;
	void HandlePlayerCollideWithWorld();

	std::vector<Duck* > &m_ducks;
	std::vector<std::unique_ptr<Player> > &m_players;	

	float m_xMin;
	float m_xMax;
	float m_yMin;
	float m_yMax;	

	QuadtreeDucks m_quadTree;
};

#endif