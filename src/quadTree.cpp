#include <iostream>
#include <memory>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

using namespace std;

#include "sphere.h"
#include "duck.h"
#include "quadTree.h"


//#define DEBUG_QUADTREE
//#define DEBUG_QUADTREE_GET_COLLIDABLES

unsigned int QuadtreeDucks::s_quadTreeMaxOjbs = 10;
unsigned int QuadtreeDucks::s_quadTreeMaxLevels = 4;


QuadtreeDucks::QuadtreeDucks(int level)
{
	m_level = level;
}
//----------------------------------------------------------------------
QuadtreeDucks::~QuadtreeDucks()
{
}
//----------------------------------------------------------------------
void QuadtreeDucks::Split()
{
	float subQuadrants[4][4] = {
		{ m_xMin, m_yMin, m_xMin + (m_xMax - m_xMin) / 2.0f, m_yMin + (m_yMax - m_yMin) / 2.0f },
		{ m_xMin + (m_xMax - m_xMin) / 2.0f, m_yMin, m_xMax, m_yMin + (m_yMax - m_yMin) / 2.0f },
		{ m_xMin + (m_xMax - m_xMin) / 2.0f, m_yMin + (m_yMax - m_yMin) / 2.0f, m_xMax, m_yMax },
		{ m_xMin , m_yMin + (m_yMax - m_yMin) / 2.0f, m_xMin + (m_xMax - m_xMin) / 2.0f, m_yMax }
	};

	#ifdef DEBUG_QUADTREE
		cout << "Split" << endl;

		cout << "Q1: [";
		for(int i = 0; i < 4; i++)
		{
			cout << subQuadrants[0][i] << ", " ;
		}
		cout << "]" << endl;

		cout << "Q2: [";
		for(int i = 0; i < 4; i++)
		{
			cout << subQuadrants[1][i] << ", " ;
		}
		cout << "]" << endl;

		cout << "Q3: [";
		for(int i = 0; i < 4; i++)
		{
			cout << subQuadrants[2][i] << ", " ;
		}
		cout << "]" << endl;

		cout << "Q4: [";
		for(int i = 0; i < 4; i++)
		{
			cout << subQuadrants[3][i] << ", " ;
		}
		cout << "]" << endl;

		
	#endif
	
	for(int i = 0; i < 4; i++)
	{
		QuadtreeDucks* child = new QuadtreeDucks(m_level + 1);
		child->SetWorldSize(subQuadrants[i][0], subQuadrants[i][1], subQuadrants[i][2], subQuadrants[i][3]);
		m_children.push_back(child);
	}	
}
//----------------------------------------------------------------------
void QuadtreeDucks::SetWorldSize(float xMin, float yMin, float xMax, float yMax)
{
	m_xMin = xMin;
	m_xMax = xMax;
	m_yMin = yMin;
	m_yMax = yMax;
}
void QuadtreeDucks::Clear()
{
	for(auto child : m_children)
	{
		delete child;
	}
	m_children.clear();
	m_ducks.clear();
}
//----------------------------------------------------------------------
void QuadtreeDucks::Insert(Duck* duck)
{
	if(!m_children.empty())
	{
		int index = GetIndex(duck->GetPosTry(), duck->GetBoundingSphereRadius());
		if(index != -1)
		{		
			m_children[index]->Insert(duck);
			return;
		}
	}

	m_ducks.push_back(duck);
	#ifdef DEBUG_QUADTREE
		cout << m_level << " added duck: " << duck->GetDebugId() << "(" << duck->GetPosTry().x << ", " << duck->GetPosTry().y << ")" << endl;
	#endif
	

	if(m_ducks.size() > s_quadTreeMaxOjbs && m_level < s_quadTreeMaxLevels)
	{		
		if(m_children.empty())
		{
			Split();

			#ifdef DEBUG_QUADTREE
				cout << m_level << " was split, trying to re-arrange ducks " << endl;
			#endif

			int i = 0;
			while(i < m_ducks.size())
			{
				int index = GetIndex(m_ducks[i]->GetPosTry(), m_ducks[i]->GetBoundingSphereRadius());
				if(index != -1)
				{				
					#ifdef DEBUG_QUADTREE
						cout << m_level << " moving duck " << m_ducks[i]->GetDebugId() << "(" << m_ducks[i]->GetPosTry().x << ", " << m_ducks[i]->GetPosTry().y << ")"
							<< " to child: " << index << endl;
					#endif
					
					m_children[index]->Insert(m_ducks[i]);
					m_ducks.erase(m_ducks.begin() + i);
				}
				else
				{
					#ifdef DEBUG_QUADTREE
						cout << m_level << " Could'nt move duck " << m_ducks[i]->GetDebugId() << "(" << m_ducks[i]->GetPosTry().x << ", " << m_ducks[i]->GetPosTry().y << ")" << endl;
					#endif
					
					i++;
				}
			}
		}
	}
	#ifdef DEBUG_QUADTREE
	else if(m_ducks.size() > s_quadTreeMaxOjbs && m_level >= s_quadTreeMaxLevels)
	{
		cout << "Not trying to re-arrange ducks because max level (" << s_quadTreeMaxLevels << ") was reached" << endl;
	}
	#endif
}
//----------------------------------------------------------------------
void QuadtreeDucks::GetChildren(std::vector<Duck* > &ducks)
{
	ducks.insert(ducks.end(), m_ducks.begin(), m_ducks.end());
	for(auto &child : m_children)
	{
		child->GetChildren(ducks);
	}
}
//----------------------------------------------------------------------
int QuadtreeDucks::GetIndex(glm::vec3 &duckPos, float boundingSphereRadius)
{
	int index = -1;

	float vertMiddle = m_xMin + (m_xMax - m_xMin) / 2.0f;
	float horzMiddle = m_yMin + (m_yMax - m_yMin) / 2.0f;

	bool topQuad = duckPos.y < horzMiddle && duckPos.y + boundingSphereRadius < horzMiddle;
	bool bottomQuadrant = duckPos.y > horzMiddle && duckPos.y - boundingSphereRadius > horzMiddle;

	#ifdef DEBUG_QUADTREE
		cout << m_level << " GetIndex vertMiddle: " << m_xMin << " + " << (m_xMax - m_xMin) << " / 2.0f = " << vertMiddle << endl;
		cout << m_level << " horzMiddle: " << m_yMin << " + " << (m_yMax - m_yMin) << " / 2.0f = " << horzMiddle << endl;	
		cout << m_level << " topQuad: " << topQuad << ", bottomQuadrant: " << bottomQuadrant << endl;
	#endif
	

	if(duckPos.x < vertMiddle && duckPos.x + boundingSphereRadius < vertMiddle)
	{
		if(topQuad)
		{
			index = 0;
		}
		else if(bottomQuadrant)
		{
			index = 3;
		}
	}
	else if(duckPos.x > vertMiddle && duckPos.x - boundingSphereRadius > vertMiddle)
	{
		if(topQuad)
		{
			index = 1;
		}
		else if(bottomQuadrant)
		{
			index = 2;
		}		
	}

	return index;
}
//----------------------------------------------------------------------
void QuadtreeDucks::GetCollidables(Duck* duck, std::vector<Duck* > &ducks)
{
	int index = GetIndex(duck->GetPosTry(), duck->GetBoundingSphereRadius());

	#ifdef DEBUG_QUADTREE_GET_COLLIDABLES
		cout << m_level << " QuadtreeDucks::GetCollidables index:" << index << endl;
	#endif

	if(index != -1 && !m_children.empty())
	{
		m_children[index]->GetCollidables(duck, ducks);
	}
	else
	{
		// We have to get all children as well since we are spanning across one of the boundaries
		for(auto &child : m_children)
		{
			child->GetChildren(ducks);
		}
	}

	/*
		There are two cases where we reach this point:
		* We reached a leaf and we should add the the m_ducks of that leaf
		* We traversed an internal node that has ducks that could not be distributed in the children (spawns the crossing lines) - we have
			to add these ducks as well since they could potentially collide with the duck we are testing for
	*/	
	#ifdef DEBUG_QUADTREE_GET_COLLIDABLES
		cout << m_level << " QuadtreeDucks::GetCollidables adding :" << m_ducks.size() << " nr of ducks" << endl;
	#endif

	ducks.insert(ducks.end(), m_ducks.begin(), m_ducks.end());
}
//----------------------------------------------------------------------
void QuadtreeDucks::DebugPrintIndent()
{
	for(int i = 0; i < m_level; i++)
	{
		cout << "   ";
	}
}
//----------------------------------------------------------------------
int QuadtreeDucks::GetNrDucks() const
{
	int nrDucks = m_ducks.size();
	for(auto child : m_children)
	{
		nrDucks += child->GetNrDucks();
	}

	return nrDucks;
}
//----------------------------------------------------------------------
bool QuadtreeDucks::VerifyNrDucks(int expectedNrDucks) const
{
	return GetNrDucks() == expectedNrDucks;
}
//----------------------------------------------------------------------	
int QuadtreeDucks::GetTreeDepth(int &level) const
{
	if(m_level > level)
	{
		level = m_level;
	}
	for(auto child : m_children)
	{
		child->GetTreeDepth(level);
	}
	return level;
}
//----------------------------------------------------------------------	
bool QuadtreeDucks::VerifyTreeDepth(int expectedDepth) const
{
	int level = 0;
	return GetTreeDepth(level) == expectedDepth;
}
//----------------------------------------------------------------------
void QuadtreeDucks::PrintTree()
{
	if(m_level == 0)
	{
		cout << "**************" << endl;
	}

	DebugPrintIndent();
	cout << "m_level: " << m_level << ", quad: (" << m_xMin << ", " << m_yMin << "), (" << m_xMax << ", " << m_yMax << ")" << endl;

	DebugPrintIndent();
	cout << "Ducks : [ " ;
	for(auto duck : m_ducks)
	{
		DebugPrintIndent();
		cout << duck->GetDebugId() << ", " <<  "(" << duck->GetPosTry().x << ", " << duck->GetPosTry().y << "), Radius: " 
			<< duck->GetBoundingSphereRadius() <<  endl;
	}
	DebugPrintIndent();
	cout << "]" << endl;
	int childCount = 0;
	for(auto &child : m_children)
	{
		DebugPrintIndent();
		cout << "Visting Q" << childCount + 1<< endl;
		child->PrintTree();
		childCount++;
	}
	
	DebugPrintIndent();
	cout << "-------------------------" << endl;

	if(m_level == 0)
	{
		cout << "**************" << endl;
	}
}
