#ifndef GAME_DEFS
#define GAME_DEFS

class GameScene;

class GameDefData
{
    public:
    GameDefData();
    ~GameDefData();

    float GetPlayerDepth() const;
    float GetReflectionOffset() const;
    float GetWaterLevel() const;

    void Tick(float deltaTime, GameScene &gameScene);

    void Reset();
    void ChangeWaterLevel(float amount);
    void AddToTimeToIncreaseFlow(float amount);
    void AddToTimeToDecreaseFlow(float amount);    

    private:

    float m_speedIncreaseFlow;
    float m_speedDecreaseFlow;

    float m_timeStartIncreaseFlow;
    float m_timeToIncreaseFlow;

    float m_timeStartDecreaseFlow;
    float m_timeToIDeceaseFlow;

    float m_speedConstantDecreaseFlow;

    float m_playerDepth;
    float m_reflectionOffset;
    float m_waterLevel;   
};

#define PLAYERDEPTH -10.0f
#define REFLECTION_OFFSET (-10.0f - PLAYERDEPTH) / 1.0f

#define WATER_LEVEL PLAYERDEPTH - 0.11f

// Duck stuff
#define DUCKSMALL 1.5f
#define DUCKMEDIUM 2.2f
#define DUCKBIG 2.9f
#define DUCKDEPTH PLAYERDEPTH


#define PROJECTILEBALLDEPTH PLAYERDEPTH


// Game world borders
#define WORLD_BORDER_OFFSET 0.25f
#define WORLD_XMIN -5.5f + WORLD_BORDER_OFFSET
#define WORLD_XMAX 5.5f - WORLD_BORDER_OFFSET
#define WORLD_YMIN -4.0f + WORLD_BORDER_OFFSET
#define WORLD_YMAX 4.0f - WORLD_BORDER_OFFSET

#endif