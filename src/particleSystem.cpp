#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <iostream>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "particleSystem.h"
#include "shader.h"

using namespace std;

GLuint ParticleSystem::m_particleVBO;
GLuint ParticleSystem::m_CameraRight_worldspaceID;
GLuint ParticleSystem::m_CameraUp_worldspaceID;
GLuint ParticleSystem::m_VPID;
GLuint ParticleSystem::m_programID;
std::vector<ParticleSystem* > ParticleSystem::m_particleSystems;

int ParticleSystem::maxParticlesTotal = 700;
int ParticleSystem::maxParticlesPerSystem =  200;


// This is quite verbose so might need to tweak it
//#define DEBUG_PARTSYS

ParticleSystem::ParticlePool& ParticleSystem::ParticlePool::GetInstance()
{
	static ParticleSystem::ParticlePool s_particlePool = ParticleSystem::ParticlePool();
	return s_particlePool;
}
//----------------------------------------------------------------------
//----------------------------------------------------------------------
ParticleSystem::ParticlePool::ParticlePool()
{
	// Create one big continuous memory section
	for(int i = 0; i < maxParticlesTotal; i++)
	{
		m_particles.push_back(Particle());
	}

	// Initially, all particles are free so lets link them all together
	for(int i = 0; i < maxParticlesTotal - 1; i++)
	{
		m_particles[i].m_currentState.m_nextFree =	&m_particles[i + 1];
	}
	m_particles[maxParticlesTotal - 1].m_currentState.m_nextFree = NULL;

	m_firstFree = &m_particles[0];
}
//----------------------------------------------------------------------
ParticleSystem::Particle* ParticleSystem::ParticlePool::CreateParticle(float *pos, float *moveDir, unsigned char *color, float size)
{
	Particle* particle = NULL;
	if(m_firstFree)
	{
		particle = m_firstFree;
		m_firstFree = m_firstFree->m_currentState.m_nextFree;
	}

	if(particle)
	{
		particle->CreateParticle(pos, moveDir, color, size);
	}

	return particle;
}
//----------------------------------------------------------------------
void ParticleSystem::ParticlePool::ReturnParticleToFreePool(Particle* p)
{
	p->m_currentState.m_nextFree = m_firstFree;
	m_firstFree = p;
}
//----------------------------------------------------------------------
ParticleSystem::ParticlePool::~ParticlePool()
{
}
//----------------------------------------------------------------------
ParticleSystem::Particle::Particle()
{
}
//----------------------------------------------------------------------
void ParticleSystem::Particle::CreateParticle(float *pos, float *moveDir, unsigned char *color, float size)
{
	memcpy(m_currentState.m_active.m_pos, pos, sizeof(float) * 3);
	memcpy(m_currentState.m_active.m_moveDir, moveDir, sizeof(float) * 3);
	memcpy(m_currentState.m_active.m_color, color, sizeof(unsigned char) * 4);
	m_currentState.m_active.m_size = size;
	m_life = 1.0f;
}
//----------------------------------------------------------------------
ParticleSystem::Particle::~Particle()
{
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------
void ParticleSystem::InitParticleSystem()
{
	const float vertex_buffer_data[] = 
	{
	-0.5f, -0.5f, 0.0f,
	0.5f, -0.5f, 0.0f,
	-0.5f, 0.5f, 0.0f,
	0.5f, 0.5f, 0.0f,
	};

	glGenBuffers(1, &m_particleVBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_particleVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_buffer_data), vertex_buffer_data, GL_STATIC_DRAW);

	m_programID = LoadShaders( "particle.vertexshader", "particle.fragmentshader" );	

	// Get handles for data in the shader
	m_CameraRight_worldspaceID = glGetUniformLocation(m_programID, "CameraRight_worldspace");
	m_CameraUp_worldspaceID = glGetUniformLocation(m_programID, "CameraUp_worldspace");
	m_VPID = glGetUniformLocation(m_programID, "VP");

	#ifdef DEBUG_PARTSYS
		cout << "m_CameraRight_worldspaceID: " << m_CameraRight_worldspaceID << endl;
		cout << "m_CameraUp_worldspaceID: "<< m_CameraUp_worldspaceID << endl;
		cout << "m_VPID: " << m_VPID << endl;
		cout << "m_programID: " << m_programID << endl;
	#endif

}
//----------------------------------------------------------------------
void ParticleSystem::PrepareRender()
{
	glUseProgram(m_programID);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);

	glVertexAttribDivisor(0, 0); // base mesh
	glVertexAttribDivisor(1, 1); // position and size
	glVertexAttribDivisor(2, 1); // color
}
//----------------------------------------------------------------------
void ParticleSystem::AfterRender()
{
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);	

	// Reset state of divisors
	glVertexAttribDivisor(0, 0);
	glVertexAttribDivisor(1, 0);
	glVertexAttribDivisor(2, 0);
		
	glDisable(GL_BLEND);	
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------

void ParticleSystem::CreateExplosion(int power, int nrParticles, glm::vec3 centerPos, float dieFactor, float slowDownFactor, 
															unsigned char *color, float size)
{	
	const glm::vec4 XAxis(1.0f, 0.0f, 0.0f, 0.0f);
	const glm::vec4 YAxis(0.0f, 1.0f, 0.0f, 0.0f);

	nrParticles = nrParticles > maxParticlesPerSystem ? maxParticlesPerSystem : nrParticles;

	m_particleSpeedSlowdownFactor = slowDownFactor;
	m_particleDieFactor = dieFactor;
	m_sortParticles = false;
	float fCenterPos[3] = { centerPos.x, centerPos.y, centerPos.z };
	for(int i = 0; i < nrParticles / 4; i++)
	{
		float rotDegrees = float(rand() % 359);

		glm::mat4 rotMat = glm::mat4(1.0f);
		rotMat = glm::rotate(rotMat, glm::radians(rotDegrees), glm::vec3(0, 0, 1));

		glm::vec4 dir1 = XAxis * rotMat;
		glm::vec4 dir2 = YAxis * rotMat;
		
		float particleDir1[3] = { dir1.x * (1.5f + float(rand() % power) * 0.1f), dir1.y * (1.5f + float(rand() % power) * 0.1f),
									0.1f + float(rand() % power) * 0.1f };
		float particleDir2[3] = { dir2.x * (1.5f + float(rand() % power) * 0.1f), dir2.y * (1.5f + float(rand() % power) * 0.1f),
									0.1f + float(rand() % power) * 0.1f };
		float particleDir1Neg[3] = { -particleDir1[0], -particleDir1[1], -particleDir1[2] };
		float particleDir2Neg[3] = { -particleDir2[0], -particleDir2[1], -particleDir2[2] };

		Particle* newParticles[5] = {
							m_particlePool.CreateParticle(fCenterPos, particleDir1, color, size),
							m_particlePool.CreateParticle(fCenterPos, particleDir1Neg, color, size),
							m_particlePool.CreateParticle(fCenterPos, particleDir2, color, size),
							m_particlePool.CreateParticle(fCenterPos, particleDir2Neg, color, size),
							NULL
						};

		#ifdef DEBUG_PARTSYS
			cout << particle->m_pos.x << ", " << particle->m_pos.y << ", " << particle->m_pos.z << endl;
			cout << static_cast<int>(particle->m_color[0]) << ", " << static_cast<int>(particle->m_color[1]) << ", " << static_cast<int>(particle->m_color[2]) << ", " << static_cast<int>(particle->m_color[3]) << endl;
		#endif 

		int j = 0;
		while(newParticles[j])
		{
			m_particles.push_back(newParticles[j]);
			j++;
		}
	}
}
//----------------------------------------------------------------------
void ParticleSystem::SetSortParticles(bool sortParticles)
{
	m_sortParticles = sortParticles;
}
//----------------------------------------------------------------------
void ParticleSystem::TickParticleSystems(float deltaTime)
{
	int i = 0;
	while(i < m_particleSystems.size())
	{
		ParticleSystem* particleSystem = m_particleSystems[i];
		bool particleSysAlive = particleSystem->Tick(deltaTime);
		if(!particleSysAlive)
		{
			delete particleSystem;
			m_particleSystems.erase(m_particleSystems.begin() + i);
		}
		else
		{
			i++;
		}
	}
}
//----------------------------------------------------------------------
void ParticleSystem::AddParticleSystem(ParticleSystem* particleSystem)
{
	m_particleSystems.push_back(particleSystem);
}
//----------------------------------------------------------------------
std::vector<ParticleSystem* >& ParticleSystem::GetParticleSystem()
{
	return m_particleSystems;
}
//----------------------------------------------------------------------
ParticleSystem::ParticleSystem() : m_particlePool( ParticleSystem::ParticlePool::GetInstance())
{
	// The VBO containing the positions and sizes of the particles
	glGenBuffers(1, &m_particlesPositionAndSizeBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_particlesPositionAndSizeBuffer);
	glBufferData(GL_ARRAY_BUFFER, maxParticlesPerSystem * 4 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);

	// The VBO containing the colors of the particles
	glGenBuffers(1, &m_particlesColorBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_particlesColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, maxParticlesPerSystem * 4 * sizeof(GLubyte), NULL, GL_STREAM_DRAW);

	// Prepare containers for the data, so as to not have to reallocate size immediatly when they are used
	m_particles.reserve(maxParticlesPerSystem);

	m_particlesPositionAndSizeData.resize(maxParticlesPerSystem * 4);
	m_particlesColorData.resize(maxParticlesPerSystem * 4);

	m_particleDieFactor = 0.5f;
	m_particleSpeedSlowdownFactor = 0.1f;
	m_sortParticles = true;
}
//----------------------------------------------------------------------
ParticleSystem::~ParticleSystem()
{
	glDeleteBuffers(1, &m_particlesPositionAndSizeBuffer);
	glDeleteBuffers(1, &m_particlesColorBuffer);

	// Return particles
	for(auto p : m_particles)
	{
		m_particlePool.ReturnParticleToFreePool(p);
	}
}
//----------------------------------------------------------------------
void ParticleSystem::SlowParticleDown(float &v, float &slowDownFactor)
{
	if(v < 0.0f)
	{
		v += slowDownFactor;
		if(v > 0.0f)
		{
			v = 0.0f;
		}
	}
	else
	{
		v -= slowDownFactor;
		if(v < 0.0f)
		{
			v = 0.0f;
		}						
	}	
}
//----------------------------------------------------------------------
bool ParticleSystem::Tick(float deltaTime)
{	
	float dieoutFactor = m_particleDieFactor * deltaTime;
	float gravityFactor = -9.8f * deltaTime;
	float particleSlowdownFactor = m_particleSpeedSlowdownFactor * deltaTime;

	bool particleSystemAlive = false;

	unsigned int particleCount = 0;
	for(auto p : m_particles)
	{
		if(p->m_life > 0.0f)
		{
			// Decrease life
			p->m_life -= dieoutFactor;
			if (p->m_life > 0.0f)
			{
				particleSystemAlive = true;
				float alphaColor = (p->m_life / 1.0f) * 255.0;
				unsigned uAlphaColor = static_cast<unsigned int>(alphaColor);
				p->m_currentState.m_active.m_color[3] = static_cast<unsigned char>(uAlphaColor);
			
				// Speed should slowly decrease
				SlowParticleDown(p->m_currentState.m_active.m_moveDir[0], particleSlowdownFactor);
				SlowParticleDown(p->m_currentState.m_active.m_moveDir[1], particleSlowdownFactor);

				// Apply a pull downwards to simulate gravity
				p->m_currentState.m_active.m_moveDir[2] += gravityFactor;

				p->m_currentState.m_active.m_pos[0] += p->m_currentState.m_active.m_moveDir[0] * deltaTime;
				p->m_currentState.m_active.m_pos[1] += p->m_currentState.m_active.m_moveDir[1] * deltaTime;
				p->m_currentState.m_active.m_pos[2] += p->m_currentState.m_active.m_moveDir[2] * deltaTime;

				m_particlesPositionAndSizeData[4 * particleCount + 0] = p->m_currentState.m_active.m_pos[0];
				m_particlesPositionAndSizeData[4 * particleCount + 1] = p->m_currentState.m_active.m_pos[1];
				m_particlesPositionAndSizeData[4 * particleCount + 2] = p->m_currentState.m_active.m_pos[2];
				m_particlesPositionAndSizeData[4 * particleCount + 3] = p->m_currentState.m_active.m_size;

				m_particlesColorData[4 * particleCount + 0] = p->m_currentState.m_active.m_color[0];
				m_particlesColorData[4 * particleCount + 1] = p->m_currentState.m_active.m_color[1];
				m_particlesColorData[4 * particleCount + 2] = p->m_currentState.m_active.m_color[2];
				m_particlesColorData[4 * particleCount + 3] = p->m_currentState.m_active.m_color[3];

				#ifdef DEBUG_PARTSYS					
					cout << "posData: " << m_particlesPositionAndSizeData[4 * particleCount + 0] << ", " << m_particlesPositionAndSizeData[4 * particleCount + 1] <<
					", " << m_particlesPositionAndSizeData[4 * particleCount + 2] << ", " << m_particlesPositionAndSizeData[4 * particleCount + 3] << endl;
					cout << "colorData: " << static_cast<int>(m_particlesColorData[4 * particleCount + 0]) << ", " << static_cast<int>(m_particlesColorData[4 * particleCount + 1]) <<
					", " << static_cast<int>(m_particlesColorData[4 * particleCount + 2]) << ", " << static_cast<int>(m_particlesColorData[4 * particleCount + 3]) << endl;					
				#endif 
				

			}
			particleCount++;
		}
		else
		{
			m_particlePool.ReturnParticleToFreePool(p);
		}	
	}

	/*
		Since we sort our particles after we updated them, but put their data into the 
		m_particlesPositionAndSizeData & m_particlesColorData before, some particles will not be correctly sorted.
		This should not effect the visual result that much, so this was made like this to make the solution a bit
		less complex and faster. If it produces graphical artifacts - make the sort before putting the data into
		m_particlesPositionAndSizeData & m_particlesColorData.
	*/
	if(m_sortParticles)
	{
		std::sort(m_particles.begin(), m_particles.end(), [](Particle* p1, Particle* p2) 
					{return p1->m_currentState.m_active.m_pos[2] > p2->m_currentState.m_active.m_pos[2];}
					);
	}
	return particleSystemAlive;
}
//----------------------------------------------------------------------
void ParticleSystem::Render(const glm::mat4 &projection, const glm::mat4 &view)
{
	// This could be done more efficent, read more on http://www.opengl.org/wiki/Buffer_Object_Streaming
	glBindBuffer(GL_ARRAY_BUFFER, m_particlesPositionAndSizeBuffer);
	glBufferData(GL_ARRAY_BUFFER, maxParticlesPerSystem * 4 * sizeof(GLfloat), NULL, GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_particles.size() * sizeof(GLfloat) * 4, &m_particlesPositionAndSizeData[0]);

	glBindBuffer(GL_ARRAY_BUFFER, m_particlesColorBuffer);
	glBufferData(GL_ARRAY_BUFFER, maxParticlesPerSystem * 4 * sizeof(GLubyte), NULL, GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, m_particles.size() * sizeof(GLubyte) * 4, &m_particlesColorData[0]);	
	
	glm::mat4 viewProjection = projection * view;
	const glm::vec3 cameraRightWorldSpace(1.0f, 0.0f, 0.0f);
	const glm::vec3 cameraUpWorldSpace(0.0f, 1.0f, 0.0f);

	// Forward to shader
	glUniformMatrix4fv(m_VPID, 1, GL_FALSE, &viewProjection[0][0]);	
	glUniform3f (m_CameraRight_worldspaceID, 	cameraRightWorldSpace[0], cameraRightWorldSpace[2], cameraRightWorldSpace[2]);
	glUniform3f (m_CameraUp_worldspaceID, 		cameraUpWorldSpace[0], cameraUpWorldSpace[1], cameraUpWorldSpace[2]);

	// 1st attrib buffer, static particle look (vertex data)	
	glBindBuffer(GL_ARRAY_BUFFER, m_particleVBO);
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);
	
	// 2nd attribute buffe, position of the particle + size (position is center and we expand the particle around this position)
	glBindBuffer(GL_ARRAY_BUFFER, m_particlesPositionAndSizeBuffer);
	glVertexAttribPointer(
		1,
		4,
		GL_FLOAT,
		GL_FALSE,
		0,
		(void*)0
	);

	// 3rd attribute buffer, color of the particle	
	glBindBuffer(GL_ARRAY_BUFFER, m_particlesColorBuffer);
	glVertexAttribPointer(
		2,
		4,
		GL_UNSIGNED_BYTE,
		GL_TRUE,			// Normalize this, so the 0-255 value is switched to 0.0f-1.0f in the shader
		0,
		(void*)0
	);

	glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, m_particles.size());
}