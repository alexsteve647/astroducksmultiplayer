#include <iostream>
using namespace std;

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "gameScene.h"
#include "model3d.h"
#include "model3d_cooktorrance.h"
#include "duck.h"
#include "player.h"
#include "projectileBall.h"
#include "particleSystem.h"
#include "gui.h"
#include "poolWalls.h"
#include "initGL.h"
#include "lightData.h"
#include "waterGfxEffect.h"
#include "renderStruct.h"
#include "gameDefs.h"

/* FOR THE HUD/GUI:

*/

GameScene::GameScene(std::vector<Duck* > &ducks, std::vector<std::unique_ptr<Player> > &players, 
						std::vector<ParticleSystem* > &particleSystems, HUDGUI* hudGUI, PoolWalls* poolWalls,
						WaterGfxEffect &waterGfxEffect, GameDefData& gameDefData, int windowWidth, int windowHeight) :
m_ducks(ducks), m_players(players), m_particleSystems(particleSystems), m_hudGUI(hudGUI), m_windowWidth(windowWidth), 
m_windowHeight(windowHeight), m_poolWalls(poolWalls), m_waterGfxEffect(waterGfxEffect), m_gameDefData(gameDefData)
{	
	m_backgroundColor = glm::vec3(0.0f, 0.41f, 0.66f);
	PrepareForRender();
}
//----------------------------------------------------------------------
GameScene::~GameScene()
{
}
//----------------------------------------------------------------------
void GameScene::PrepareForRender()
{
	// construct projection matrix
	const float fov = 45.0f;
	float nearZ = 0.1f;
	float farZ = 100.0f;
	m_projection = glm::perspective(glm::radians(fov), (float)4.0f / (float)3.0f, nearZ, farZ);

	//	Create a orthographic projection. This is convenient when doing GUI things, since we are working in 2D. We will now have a canvas
	//	the size of (m_windowWidth, m_windowHeight) and we just ignore the Z axis.
	m_hudOrthoMVP = glm::ortho(0.0f, static_cast<float>(m_windowWidth), static_cast<float>(m_windowHeight), 0.0f);

	InitLightData(m_sceneLight);

	RecalculateWaterMatrices();
}
//----------------------------------------------------------------------
void GameScene::RecalculateWaterMatrices()
{
	const float fov = 45.0f;
	float nearZ = 0.1f;
	float farZ = 100.0f;

	double waterLevelHeight = -m_gameDefData.GetPlayerDepth() + 1.11f + 5.0f;
	m_clipPlane = glm::vec4(0.0f, 0.0f, 1.0f, waterLevelHeight);

	/*
		The view matrix will move the WORLD around the camera, not the otherway around. To move the camera 3 units to the
		right, we would need to move the WORLD 3 units to the left. Important to remember when working with the view matrix
		--------------------------------------------------------------------------------------------------------------------

		To render the reflection, we place the camera behind the water plane facing straight up along the positive
		Z-axis.
		X and Y should be selected so that the camera is sitting in the middle of the water.

		To make it appear as if the camera is facing the positive Z-axis, we instead invert the world by applying a
		scaling matrix (1, 1, -1) (As mentioned at the top, we cant move the camera but instead we move the world
		around the camera).
	*/	

	const double waterZAxis = (m_gameDefData.GetPlayerDepth() * 2.0f) + m_gameDefData.GetReflectionOffset();

	
	// Lets assume WORLD_XMAX is always positive
	double worldXLength = 0.0f;
	if(WORLD_XMIN < 0)
	{
		worldXLength = WORLD_XMAX + fabs(WORLD_XMIN);
	}
	else
	{
		worldXLength = WORLD_XMAX - WORLD_XMIN;
	}

	double worldYLength = 0.0f;
	if(WORLD_YMIN < 0)
	{
		worldYLength = WORLD_YMAX + fabs(WORLD_YMIN);
	}
	else
	{
		worldYLength = WORLD_YMAX - fabs(WORLD_YMIN);
	}

	const double worldXCenter = WORLD_XMIN + worldXLength / 2.0f;
	const double worldYCenter = WORLD_YMIN + worldYLength / 2.0f;	

	// We cant move the camera to worldXCenter, worldYCenter - but we can move the world around
	// us. That is why we negate the worldXCenter, worldYCenter values
	m_viewWaterCameraReflection = glm::mat4(1.0f);
	m_viewWaterCameraReflection = glm::translate(m_viewWaterCameraReflection, 
													glm::vec3(-worldXCenter, -worldYCenter, waterZAxis));	

	m_viewWaterCameraReflection = glm::scale(m_viewWaterCameraReflection, glm::vec3(1.0f, 1.0f, -1.0f));
	
	// Depending on the shape of the water surface, we might need to adjust the aspect ratio.
	// If it is shaped like a rectangle with X being a bit longer than Y, than a 4/3 aspect ratio
	// is fine. But if that changes we also need to fit our projection matrix so that water cameras
	// viewing frustum reflects this.
	m_projectionWaterCameraReflection = glm::perspective(glm::radians(fov), float(worldXLength) / float(worldYLength), 
															nearZ, farZ);
}
//----------------------------------------------------------------------
void GameScene::RenderModel3DObjects(const glm::mat4 &view, const glm::mat4 &projection, bool invertLightPos) const
{
	const RenderStateModel3D model3DRenderState = {m_sceneLight, m_clipPlane, invertLightPos};

	#ifdef USE_PBR
		Model3dCookTorrance::PrepareRender();
	#else
		Model3d::PrepareRender();
	#endif
	
	for(auto &duck : m_ducks)
	{
		duck->Render(model3DRenderState, projection, view);
	}	

	for(auto &player : m_players)
	{
		if(player->GetHealth() > 0)
		{
			player->Render(model3DRenderState, projection, view);
			for(auto &projectileBall : player->GetProjectileBalls())
			{
				projectileBall->Render(model3DRenderState, projection, view);
			}
		}
	}

	#ifdef USE_PBR
		Model3dCookTorrance::AfterRender();
	#else
		Model3d::AfterRender();
	#endif

}
//----------------------------------------------------------------------
void GameScene::RenderScene() const
{
	RenderScene(PlayerViewMatrix(), PlayerProjectionMatrix());
}
void GameScene::RenderScene(const glm::mat4 &view, const glm::mat4 &projection) const
{	
	m_waterGfxEffect.CreateReflectionTexture(this);
	
	// Set background color
	glClearColor(m_backgroundColor.x, m_backgroundColor.y, m_backgroundColor.z, 0.0f);
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	RenderSceneWithoutHUD(view, projection, true, true, false);

	// Render water
	const RenderStateTexturedQuad3D texturedQuad3DRenderState = {m_sceneLight, m_clipPlane, true, false};
	m_waterGfxEffect.Render(texturedQuad3DRenderState, projection, view);	

	m_hudGUI->Render(m_hudOrthoMVP, view);
}
//----------------------------------------------------------------------
const glm::mat4& GameScene::PlayerViewMatrix() const
{
	return m_view;
}
//----------------------------------------------------------------------
const glm::mat4& GameScene::WaterViewMatrix() const
{
	return m_viewWaterCameraReflection;
}
//----------------------------------------------------------------------
const glm::mat4& GameScene::PlayerProjectionMatrix() const
{
	return m_projection;
}
//----------------------------------------------------------------------
const glm::mat4& GameScene::WaterProjectionMatrix() const
{
	return m_projectionWaterCameraReflection;
}
//----------------------------------------------------------------------
void  GameScene::RenderSceneWithoutHUD(const glm::mat4 &view, const glm::mat4 &projection, bool renderPoolWalls,
										bool renderParticleEffects, bool invertLightPos) const
{
	if(renderPoolWalls)
	{
		const RenderStateTexturedQuad3D texturedQuad3DRenderState = {m_sceneLight, m_clipPlane, false, false};
		m_poolWalls->Render(texturedQuad3DRenderState, projection, view);	
	}

	RenderModel3DObjects(view, projection, invertLightPos);

	if(renderParticleEffects)
	{
		ParticleSystem::PrepareRender();
		for(auto &particleSystem : m_particleSystems)
		{
			particleSystem->Render(projection, view);
		}
		ParticleSystem::AfterRender();
	}
}
