#include <algorithm>
#include <iostream>
#include "aStarPathFind.h"

using namespace std;

vector<std::unique_ptr<NodeAStar> > NodeAStar::m_worldMap;

#define DEBUG_PATH_FIND

void NodeAStar::InitAStarPathFind()
{
	for(int i = 0; i < MAX_STEP_Y; i++)
	{
		for(int j = 0; j < MAX_STEP_X; j++)
		{
			unique_ptr<NodeAStar> current = make_unique<NodeAStar>();
			current->m_x = j;
			current->m_y = i;
			current->m_parent = NULL;
			current->m_gCost = 0;
			current->m_hCost = 0;
			current->m_fCost = 0;
			current->m_traversable = true;
			m_worldMap.push_back(move(current));

			#ifdef DEBUG_PATH_FIND
				int index = m_worldMap.size() - 1;
				cout << "m_worldMap[" << index << "] (" << m_worldMap[index].get()->m_x << ", " << m_worldMap[index].get()->m_y << ")" << endl;
			#endif
		}
	}
}

// "Manhattan" distance
int calculateManhattanDistance(NodeAStar* nodeA, NodeAStar* nodeB)
{
	int gCostX = abs(nodeA->m_x - nodeB->m_x);
	int gCostY = abs(nodeA->m_y - nodeB->m_y);

	return gCostX + gCostY;
}

void calculatePath(std::vector<NodeAStar*> &openList, std::vector<NodeAStar*> &closedList, NodeAStar* currentNode, NodeAStar* goalNode)
{
	#ifdef DEBUG_PATH_FIND
		int processingNodeIndex = currentNode->m_y * MAX_STEP_X + currentNode->m_x;
		cout << "Processing node (" << currentNode->m_x << ", " << currentNode->m_y << ") index: " << processingNodeIndex << endl;
	#endif
	
	// Add start point to closedList
	closedList.push_back(currentNode);

	// Check if we reached the goalNode
	std::vector<NodeAStar*>::iterator itGoalNodeInClosedList = std::find(closedList.begin(), closedList.end(), goalNode );

	if(itGoalNodeInClosedList != closedList.end())
	{
		#ifdef DEBUG_PATH_FIND
			cout << "goal node was found" << endl;
		#endif

		/* Walk us backwards to the start - this is the path */
		NodeAStar* a = currentNode;
		while(a->m_parent)
		{
			cout << "(" << a->m_x << ", " << a->m_y << ")" << endl;
			a = a->m_parent;
		}
		
		return;
	}

	#ifdef DEBUG_PATH_FIND
		cout << "closedList: " << endl;
		for(auto node : closedList)
		{
			cout << "node (" << node->m_x << ", " << node->m_y << ")" << endl;
		}
		cout << "-------------" << endl;
	#endif
	

	// Fetch and calculate adjacent nodes cost
	std::vector<NodeAStar*> adjacent;

	int indexWorldMapStartNode = currentNode->m_y * MAX_STEP_X + currentNode->m_x;

	bool borderTop = currentNode->m_y == 0;
	bool borderLeft = currentNode->m_x == 0;
	bool borderRight = currentNode->m_x == MAX_STEP_X - 1;
	bool borderBottom = currentNode->m_y == MAX_STEP_Y - 1;

	#ifdef DEBUG_PATH_FIND
		cout << "borderTop " << borderTop << ", borderLeft " << borderLeft << ", borderRight " << borderRight << 
				", borderBottom " << borderBottom << endl;
	#endif
	

	// Top to bottom, left to right
	int i = borderTop ? 1 : 0;
	int iMax = borderBottom ? 2 : 3;
	int jMax = borderRight ? 2 : 3;

	// Fetch all traversable path adjacent to the node we are considering
	while(i < iMax)
	{
		int j = borderLeft ? 1 : 0;
		while(j < jMax)
		{
			int x = currentNode->m_x + (j - 1);
			int y = currentNode->m_y + (i - 1);
			int indexWorldMap = y * MAX_STEP_X + x;

			NodeAStar* node = NodeAStar::m_worldMap[indexWorldMap].get();

			if(currentNode != node)
			{
				if(node->m_traversable)
				{
					#ifdef DEBUG_PATH_FIND
						cout << "Fetching (" << x << ", " << y << ") at index : " << indexWorldMap << endl;
					#endif					
					adjacent.push_back(node);
				}
			}
			j++;
		}
		i++;
	}	

	// Calculate H & G cost for adjacents
	const int lengthFactor = 10;
	const int lengthFactorDiagonal = 15;
	for(auto node : adjacent)
	{
		std::vector<NodeAStar*>::iterator itClosed = std::find(closedList.begin(), closedList.end(), node );

		#ifdef DEBUG_PATH_FIND
			if(itClosed != closedList.end())
			{
				cout << "node (" << node->m_x << ", " << node->m_y << ") was in closed list, continuing" << endl;
			}
		#endif
		
		if(itClosed == closedList.end())
		{
			#ifdef DEBUG_PATH_FIND
				cout << "node (" << node->m_x << ", " << node->m_y << ") not in closed" << endl;
			#endif

			bool isDiagonalMove = (node->m_y != currentNode->m_y && node->m_x != currentNode->m_x);
			int factor = isDiagonalMove ? lengthFactorDiagonal : lengthFactor;
			
			std::vector<NodeAStar*>::iterator itOpenList = std::find(openList.begin(), openList.end(), node );

			// If not  already in open list add it
			if(itOpenList == openList.end())
			{			
				#ifdef DEBUG_PATH_FIND
					cout << "node (" << node->m_x << ", " << node->m_y << ") not in openlist" << endl;					
				#endif
			
				node->m_parent = currentNode;
				node->m_gCost = node->m_parent->m_gCost + factor;
				node->m_hCost = calculateManhattanDistance(goalNode, node) * lengthFactor;
				node->m_fCost = node->m_gCost + node->m_hCost;

				#ifdef DEBUG_PATH_FIND
					cout << "node " <<  node->m_x << ", " << node->m_y << ") index gCost: " << node->m_gCost << ", hcost: " << node->m_hCost << ", fcost: " << node->m_fCost << endl;
				#endif				
				openList.push_back(node);				


			}
			else
			{

				#ifdef DEBUG_PATH_FIND
					cout << "node (" << node->m_x << ", " << node->m_y << ") was found in openlist" << endl;					
				#endif
				
				// recalculate fCost - if it is lower than the stored value, update the node and its parent as well cause this is a better path
				int gCost = currentNode->m_gCost + factor;
				int hCost = calculateManhattanDistance(goalNode, node) * lengthFactor;
				int fCost = gCost + hCost;
				if(fCost < node->m_fCost)
				{
					#ifdef DEBUG_PATH_FIND
						cout << "new fCost lower, updating" << endl;					
					#endif
					
					node->m_gCost = gCost;
					node->m_hCost = hCost;
					node->m_fCost = fCost;
					node->m_parent = currentNode;
				}
			}
		}
	}
	
	// Look at the openList and look at the one with the lowest cost and start exploring that one
	#ifdef DEBUG_PATH_FIND
		cout << "Openlist:" << endl;					
	#endif
	
	i = 0;
	int lowestCost = 99999;
	int indexLowest = -1;
	while(i < openList.size())
	{
		#ifdef DEBUG_PATH_FIND
			NodeAStar* theNode = openList[i]; 
			cout << "node (" << theNode->m_x << ", " << theNode->m_y << ")" << endl;	

			cout << "lowestCost: " << lowestCost << ", node cost: " << theNode->m_fCost << endl;
		#endif
		
		if(openList[i]->m_fCost < lowestCost)
		{
			lowestCost = openList[i]->m_fCost;
			indexLowest = i;
		}
		i++;
	}
	#ifdef DEBUG_PATH_FIND
		cout << "indexLowest: " << indexLowest << endl;
		cout << "***********************" << endl;					
	#endif


	if(indexLowest > -1)
	{
		NodeAStar* nextNode = openList[indexLowest];
		openList.erase(openList.begin() + indexLowest);
		calculatePath(openList, closedList, nextNode, goalNode);
	}
}

// Assumes m_worldMap has been updated so that squares that are not traversable have had their boolean set
void findPath(NodeAStar *startNode, NodeAStar *goalNode, std::vector<NodeAStar*> path)
{
	std::vector<NodeAStar*> openList;
	std::vector<NodeAStar*> closedList;

	calculatePath(openList, closedList, startNode, goalNode);
}
//----------------------------------------------------------------------
