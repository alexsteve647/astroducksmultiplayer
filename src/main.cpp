// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>

#if defined(_WIN32)
	#include <time.h>
#endif

// For multiplayer we need tcp/ip support
#include <sys/types.h> //contains a number of basic derived types that should be used whenever appropriate
#include <arpa/inet.h> // defines in_addr structure
#include <sys/socket.h> // for socket creation
#include <netinet/in.h> //contains constants and structures needed for internet domain addresses
 

using namespace std;

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include "shader.h"
#include "initGL.h"
#include "handleInput.h"
#include "model3d.h"
#include "model3d_cooktorrance.h"
#include "gameScene.h"
#include "duck.h"
#include "player.h"
#include "projectileBall.h"
#include "collisionDetection.h"
#include "collisionSystem.h"
#include "particleSystem.h"
#include "gui.h"
#include "texturedQuad.h"
#include "texturedQuad3D.h"
#include "texture.h"
#include "text2D.h"
#include "inGameHUD.h"
#include "poolWalls.h"
#include "waterGfxEffect.h"
#include "gameDefs.h"

#include "aStarPathFind.h"

void DisplayStartupText()
{
	std::string line;
	ifstream startUpText ("startupText.txt");
	if (startUpText.is_open())
	{
		while (getline (startUpText, line) )
		{
			cout << line << endl;
		}
		startUpText.close();
	}	
}

// Spawn a bunch of random ducks
void SpawnDucks(std::vector<Duck*> &ducks, int nrDucks, Player* player, const GameDefData* gameDefData)
{
	glm::vec3 playerPos = player->GetPos();
	for(int i = 0; i < nrDucks; i++)
	{
		bool samePos = false;
		float duckPosX = 0.0f;
		float duckPosY = 0.0f;
		
		bool placeLeft = rand() % 2 == 0 ? true : false;
		bool placeTop = rand() % 2 == 0 ? true : false;

		if(placeLeft)
		{
			duckPosX =  playerPos.x - 0.9f - float(rand() % 4);
		}
		else
		{
			duckPosX = playerPos.x + 0.9f + float(rand() % 4);
		}

		if(placeTop)
		{
			duckPosY = playerPos.y + 0.9f + float(rand() % 3);
		}
		else
		{
			duckPosY = playerPos.y - 0.9f - float(rand() % 3);
		}

		Duck* duck = Duck::DuckPool::GetInstance().GetDuck();
		
		int duckSize = rand() % 3;
		duckSize = 2;
		duck->SetDuckSize(duckSize + 1);
		if(duckSize == 0)
		{
			duck->SetScale(DUCKSMALL);			
		}
		else if(duckSize == 1)
		{
			duck->SetScale(DUCKMEDIUM);
		}
		else
		{
			duck->SetScale(DUCKBIG);
		}

		duck->SetPos(glm::vec3(duckPosX, duckPosY, DuckDepth(duckSize, gameDefData)));

		int j = 0;
		while(j < ducks.size())
		{
			Duck* aDuck = ducks[j];
			
			if(aDuck != duck)
			{
				bool collision = SphereSphereTest(duck->GetPos(), duck->GetBoundingSphereRadius(), aDuck->GetPos(), 
									aDuck->GetBoundingSphereRadius());
				if(collision)
				{
					duckPosX += 0.2f;
					duckPosY += 0.2f;

					duckPosX += 0.2f * float(rand() % 10);
					duckPosY += 0.2f * float(rand() % 10);
					duck->SetPos(glm::vec3(duckPosX, duckPosY, DuckDepth(duckSize, gameDefData)));
					j = 0;
					//cout << "fixed a duck" << endl;
				}
				else
				{
					j++;
				}
			}
		}

		duck->SetRot(glm::vec3(90.0f, 0.0f, 0.0f));

		float duckAngle = float(rand() % 360);
		duck->SetFacingRotationImmediate(duckAngle);
		ducks.push_back(duck);
	}

}

void ResetGame(std::vector<std::unique_ptr<Player> > &players, std::vector<Duck* > &ducks, double &timeStartGame, int &nrDucksToCreate,
				GameDefData& gameDefData)
{
	for(auto &player : players)
	{
		player->ResetHealth();
		player->ResetScore();
		player->SetPos(glm::vec3(0.0f, 0.0f, gameDefData.GetPlayerDepth()));
	}

	for(auto &duck : ducks)
	{
		Duck::DuckPool::GetInstance().ReturnDuckToFreePool(duck);
	}
	ducks.clear();

	nrDucksToCreate = 10;
	SpawnDucks(ducks, nrDucksToCreate, players[0].get(), &gameDefData);

	gameDefData.Reset();

	timeStartGame = glfwGetTime();
}

bool PlayersDead(const std::vector<std::unique_ptr<Player> > &players)
{
	bool bothPlayersDead = false;
	bool playerOneDead = players[0]->GetHealth() == 0;
	bool playerTwoDead = players.size() == 2 ? players[1]->GetHealth() == 0 : true;
	return playerOneDead && playerTwoDead;
}

bool ShouldUpdateTime(const std::vector<std::unique_ptr<Player> > &players, int minutesLeft, int secondsLeft)
{
	bool bothPlayersDead = PlayersDead(players);

	bool retValue = false;
	if(!bothPlayersDead && !(minutesLeft == 0  && secondsLeft == 0))
	{
		retValue = true;
	}

	return retValue;
}

int WaitForPlayerToJoin()
{
	int clintListn = 0, socketForClient = 0;
	struct sockaddr_in ipOfServer;
	clintListn = socket(AF_INET, SOCK_STREAM, 0); // creating socket
	memset(&ipOfServer, '0', sizeof(ipOfServer));
	ipOfServer.sin_family = AF_INET;
	ipOfServer.sin_addr.s_addr = htonl(INADDR_ANY);
	ipOfServer.sin_port = htons(2017); // this is the port number of running server
	bind(clintListn, (struct sockaddr*)&ipOfServer , sizeof(ipOfServer));
	listen(clintListn , 1); // Start listening

	socketForClient = accept(clintListn, (struct sockaddr*)NULL, NULL);
	return socketForClient;
}

/*
	Home-brewn itoa, since itoa is not default.
	Max length of value is 20 digits
*/
long Jimltoa(char *buffOut, int bufferSize, long value)
{
	//if(value <= 9999999)
	{
		/*
			Will output the value in reverse to buff, then we simply flip it
		*/
		char buff[20];
		buff[bufferSize - 1] = '\0';
		long tmp = value;
		long i = 0;
		while(tmp > 0)
		{
			char c = tmp % 10 + '0';
			buff[i] = c;
			i++;
			tmp = tmp / 10;
		}

		i = 1;
		while(i < bufferSize - 1)
		{
			buffOut[(bufferSize -1) - i] = buff[i];
			i++;
		}
	}


}

int main(int argc, char** argv) 
{

	// Support some simple stats to be displayed when running the game
	bool showFrameTime = false;
 	for (int i = 0; i < argc; i++)
	{
		std::string inArg = argv[i];

		if(!inArg.compare("-frametime"))
		{
			showFrameTime = true;
		}
	}

	long currentTime = static_cast<long>(time(NULL));

	srand(currentTime); // Make sure we get new random values from rand

	GLFWwindow* window;

	DisplayStartupText();

	cout << "Waiting for clients to join on port 2017..." << endl;

	int socketForClient = WaitForPlayerToJoin();

	constexpr int dataSendSize = 256;
	char dataSend[dataSendSize];
	memset(dataSend, '\0' ,dataSendSize);

	cout << "Player joined at socket " << socketForClient << endl;
	cout << "Sending seed nr for rnd: " << currentTime << endl;

	memcpy(&dataSend[0], &currentTime, sizeof(time_t));
	send(socketForClient, dataSend, sizeof(time_t), 0);

	getchar(); // Wait for user to press Enter

	if(InitGL(&window, s_windowWidth, s_windowHeight, "AstroDucks Server") == -1)
	{
		return -1;
	}

	GLuint vertexArrayID;
	glGenVertexArrays(1, &vertexArrayID);
	glBindVertexArray(vertexArrayID);

	InputState inputState;

	// GAMEDEF DATA
	GameDefData theGameDefData;

	Model3d::InitModel3d();
	Model3dCookTorrance::InitModel3dCookTorrance();
	Duck::InitDucks(&theGameDefData);
	Text2D::InitText2D();


	// PLAYER STUFF
	Player::InitPlayer();
	std::unique_ptr<Player> playerOne = make_unique<Player>(false, theGameDefData);
	playerOne->SetPos(glm::vec3(0.0f, 0.0f, theGameDefData.GetPlayerDepth()));
	playerOne->SetRot(glm::vec3(90.0f, 0.0f, 0.0f));
	playerOne->SetFacingRotationImmediate(0.0f);
	playerOne->SetColor(glm::vec3(0.7f, 0.0f, 0.0f));
	playerOne->SetScale(0.085f);	

	std::unique_ptr<Player> playerTwo = make_unique<Player>(true, theGameDefData);
	playerTwo->SetPos(glm::vec3(1.0f, 0.0f, theGameDefData.GetPlayerDepth()));
	playerTwo->SetRot(glm::vec3(90.0f, 0.0f, 0.0f));
	playerTwo->SetFacingRotationImmediate(0.0f);
	playerTwo->SetColor(glm::vec3(0.7f, 0.0f, 0.0f));
	playerTwo->SetScale(0.085f);

	std::vector<std::unique_ptr<Player> > players;
	players.push_back(std::move(playerOne));
	players.push_back(std::move(playerTwo));

	int nrDucksToCreate = 10;
	std::vector<Duck* > ducks;
	SpawnDucks(ducks, nrDucksToCreate, players[0].get(), &theGameDefData);
	
	CollisionEngine &collisionEngine = CollisionEngine::GetInstance(ducks, players);

	ProjectileBall::InitProjectileBall();
	ParticleSystem::InitParticleSystem();
	TexturedQuad::InitTexturedQuad();
	TexturedQuad3D::InitTexturedQuad3D();

	// In game HUD
	bool singlePlayer = players.size() == 1 ? true : false;
	InGameHUD gameHUD(singlePlayer);
	gameHUD.ShowTimeMainLoopTime(showFrameTime);


	PoolWalls thePoolWalls;
	WaterGfxEffect waterGfxEffect(theGameDefData);

	GameScene gameScene(ducks, players, ParticleSystem::GetParticleSystem(), gameHUD.GetGameHud(), &thePoolWalls,
						waterGfxEffect, theGameDefData, s_windowWidth, s_windowHeight);

	// Game timer
	double timeStartGame = glfwGetTime();
	double gameTimeToClearLevel = 90.0f;

	// Restart game
	bool restartGame = false;

	double lastTime = glfwGetTime();

	while(glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(window) == 0)
	{
		glfwPollEvents();

		// Time difference between frames
		double currentTime = glfwGetTime();
		float deltaTime = float(currentTime - lastTime);

		Duck::CollisionTick(ducks, deltaTime);
		Player::CollisionTick(players, deltaTime);

		CollisionResult collisionResult;
		collisionEngine.Tick(&theGameDefData, collisionResult);

		float increaseWaterTot = collisionResult.m_increaseWaterLevel - collisionResult.m_decreaseWaterLevel;
		if(increaseWaterTot > 0.0f)
		{
			theGameDefData.AddToTimeToIncreaseFlow(increaseWaterTot);
		}
		if(increaseWaterTot < 0.0f)
		{
			theGameDefData.AddToTimeToDecreaseFlow(collisionResult.m_decreaseWaterLevel);
		}		
		

		players[0]->Tick(deltaTime);
		if(players.size() == 2)
		{
			players[1]->Tick(deltaTime);
		}

		Duck::Tick(ducks, deltaTime);
		ParticleSystem::TickParticleSystems(deltaTime);		
		waterGfxEffect.Tick();
		theGameDefData.Tick(deltaTime, gameScene);	
	
		gameHUD.UpdatePlayerHealth(players[0]->GetHealth());
		gameHUD.UpdatePlayerScore(players[0]->GetScore());

		if(players.size() == 2)
		{
			gameHUD.UpdatePlayerTwoHealth(players[1]->GetHealth());
			gameHUD.UpdatePlayerTwoScore(players[1]->GetScore());
		}

		double timePassed = gameTimeToClearLevel - (currentTime - timeStartGame);
		int minutesLeft = static_cast<int>(timePassed / 60);
		int secondsLeft = static_cast<int>(timePassed - static_cast<float>(minutesLeft) * 60.0f);

		if(ShouldUpdateTime(players, minutesLeft, secondsLeft))
		{
			gameHUD.UpdateTimeLeft(minutesLeft, secondsLeft);
		}
		gameHUD.SetMainLoopTime(deltaTime);

		gameHUD.PrepareHUDGUIForRender();

		bool gameOver = PlayersDead(players) || (minutesLeft <= 0  && secondsLeft <= 0) || theGameDefData.GetWaterLevel() <= -14.7f;
		HandleKeyInput(window, inputState, deltaTime, players[0].get(), players.size() == 2 ? players[1].get() : NULL, restartGame,
						gameOver);

		gameScene.RenderScene(); 
		
		glfwSwapBuffers(window);

		if(ducks.empty())
		{
			// If one player died, bring him back but give him only 2 lifes at start and reduce his score by 200
			for(auto &player : players)
			{
				if(player->GetHealth() == 0 )
				{
					player->ResetHealth(2);
					player->DecreaseScore(200);
				}
			}

			nrDucksToCreate++;
			SpawnDucks(ducks, nrDucksToCreate, players[0].get(), &theGameDefData);
			timeStartGame = glfwGetTime();
		}

		// Handle server/client communication

		// Send player one position and orientation to client
		memset(dataSend, '\0', dataSendSize);
		memcpy(&dataSend[0 * sizeof(float)], &players[0].get()->GetPos().x, sizeof(float));
		memcpy(&dataSend[1 * sizeof(float)], &players[0].get()->GetPos().y, sizeof(float));
		memcpy(&dataSend[2 * sizeof(float)], &players[0].get()->GetPos().z, sizeof(float));


		memcpy(&dataSend[3 * sizeof(float)], &players[0].get()->GetRot().x, sizeof(float));
		memcpy(&dataSend[4 * sizeof(float)], &players[0].get()->GetRot().y, sizeof(float));
		memcpy(&dataSend[5 * sizeof(float)], &players[0].get()->GetRot().z, sizeof(float));

		send(socketForClient, dataSend, sizeof(float) * 6, 0);

		if(gameOver)
		{
			gameHUD.ShowGameOverSign(true);

			if(restartGame)
			{
				ResetGame(players, ducks, timeStartGame, nrDucksToCreate, theGameDefData);
				gameHUD.ShowGameOverSign(false);				
			}
		}		

		lastTime = currentTime;
	}

	ClearTextureCache();

	// Clean up and close OpenGL
	glDeleteVertexArrays(1, &vertexArrayID);	
	glfwTerminate();

	shutdown(socketForClient, SHUT_RDWR);



	return 0;
}

