#ifndef RENDER_STRUCT
#define RENDER_STRUCT


struct RenderStateTexturedQuad3D
{
    const SceneLightData &m_sceneLightData;
    const glm::vec4 &m_clipPlane;
    bool        m_blendOn;
    bool        m_disableDepthTest;
};


struct RenderStateModel3D
{
    const SceneLightData &m_sceneLightData;
    const glm::vec4 &m_clipPlane;
    bool            m_invertLightPos;
};

#define USE_PBR


#endif 