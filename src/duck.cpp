#include <iostream>
#include <fstream>
using namespace std;
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "model3d.h"
#include "model3d_cooktorrance.h"
#include "duck.h"
#include "collisionDetection.h"
#include "particleSystem.h"
#include "lightData.h"
#include "renderStruct.h"
#include "texture.h"
#include "gameDefs.h"

//#define DEBUG


Model3d* Duck::m_model = NULL;
Model3dCookTorrance* Duck::m_modelCookTorrance = NULL;
int Duck::m_debugID = 0;

glm::mat4 Duck::m_rotationSplitDuckRight;
glm::mat4 Duck::m_rotationSplitDuckLeft;
const GameDefData* Duck::m_gameDefData;
bool Duck::m_hasDestroyDucks;
std::vector<Duck*> Duck::m_newDucks;

#if defined(_WIN32)
	#define M_PI 3.14159f
#endif

float DuckDepth(int duckSize, const GameDefData* gameDefData)
{
	if(duckSize == 2)
	{
		return gameDefData->GetPlayerDepth();
	}
	else if(duckSize == 1)
	{
		return gameDefData->GetPlayerDepth() + 0.1f;
	}
	else if(duckSize == 0)
	{
		return gameDefData->GetPlayerDepth() + 0.2f;
	}

	return gameDefData->GetPlayerDepth();
}

//----------------------------------------------------------------------
void Duck::InitDucks(const GameDefData* gameDefData)
{
	m_hasDestroyDucks = false;
	m_gameDefData = gameDefData;
	LoadModel();
	CreateSplitMatrices();	
}
//----------------------------------------------------------------------
void Duck::LoadModel()
{
	m_model = new Model3d();
	m_model->Build("rubberDuck.obj");

	m_modelCookTorrance = new Model3dCookTorrance();
	m_modelCookTorrance->Build("rubberDuck.obj");

	//glm::vec3 duckColor(1.0f, 0.78f ,0.0f);
	//m_model->SetColor(duckColor);

	m_model->SetTexture(GetTextureResource(DUCK_PLASTIC_ALBEDO_TEXTURE));
	m_modelCookTorrance->SetTexture(GetTextureResource(DUCK_PLASTIC_ALBEDO_TEXTURE),	GetTextureResource(DUCK_PLASTIC_NORMAL_TEXTURE),
									GetTextureResource(DUCK_PLASTIC_METALLIC_TEXTURE),	GetTextureResource(DUCK_PLASTIC_ROUGHNESS_TEXTURE),
									GetTextureResource(DUCK_PLASTIC_AO_TEXTURE));
}
//----------------------------------------------------------------------
void Duck::CreateSplitMatrices()
{
	const float splitOffAngle = 135.0f;
	m_rotationSplitDuckRight = glm::mat4(1.0f); // Identity matrix
	m_rotationSplitDuckRight = glm::rotate(m_rotationSplitDuckRight, glm::radians(-splitOffAngle), glm::vec3(0.0f, 0.0f, 1.0f));

	m_rotationSplitDuckLeft = glm::mat4(1.0f); // Identity matrix
	m_rotationSplitDuckLeft = glm::rotate(m_rotationSplitDuckLeft, glm::radians(splitOffAngle), glm::vec3(0.0f, 0.0f, 1.0f));

}
//----------------------------------------------------------------------
bool Duck::DuckCollidesWithOtherDucks(Duck* duck, std::vector<Duck* >&otherDucks)
{
	int k = 0;
	bool newDuckCollidesWithWorldDucks = false;

	while(k < otherDucks.size() && !newDuckCollidesWithWorldDucks)
	{
		Duck* existingDuck = otherDucks[k];
		newDuckCollidesWithWorldDucks = SphereSphereTest(existingDuck->GetPos(), existingDuck->GetBoundingSphereRadius(), 
							duck->GetPos(), duck->GetBoundingSphereRadius());
		
		k++;
	}

	return newDuckCollidesWithWorldDucks;	
}
//----------------------------------------------------------------------
bool Duck::DuckInsideWorldBorder(Duck* duck)
{
	float worldBorderOffset = 0.25f;
	float xMin = -5.5f + worldBorderOffset;
	float xMax =  5.5f - worldBorderOffset;
	float yMin = -4.0f + worldBorderOffset;
	float yMax =  4.0f - worldBorderOffset;

	glm::vec3 duckPos = duck->GetPos();
	if(duckPos.x - duck->GetBoundingSphereRadius() < xMin || duckPos.x + duck->GetBoundingSphereRadius() > xMax
		|| duckPos.y - duck->GetBoundingSphereRadius() < yMin  || duckPos.y + duck->GetBoundingSphereRadius() > yMax)
	{
		return false;
	}

	return true;
}
//----------------------------------------------------------------------
void Duck::Destroy(Duck* duck, const GameDefData* gameDefData, DuckDestroyResult &result)
{
	m_hasDestroyDucks = true;

	duck->Destroy();
	const glm::vec3 positiveZAxis(0.0f, 0.0f, 1.0f);

	ParticleSystem* partSys = new ParticleSystem();
	
	// Generate a particle explosion
	int nrParticles = 20 * duck->GetDuckSize();
	float particleSize = 0.023f * duck->GetDuckSize();

	unsigned char color[4];
	switch(duck->GetDuckTexturePack())
	{
		case 0: // Red
			color[0] = 255; color[1] = 0; color[2] = 0; color[3] = 255;			
		break;

		case 1: // Blue
			color[0] = 0; color[1] = 0; color[2] = 255; color[3] = 255;
		break;

		case 2: // Black
			color[0] = 0; color[1] = 0; color[2] = 0; color[3] = 255;
		break;

		case 3: // Yellow
			color[0] = 255; color[1] = 199; color[2] = 0; color[3] = 255;			
		break;

	};

	partSys->CreateExplosion(30, nrParticles, duck->GetPos(), 1.2f, 2.8f, color, particleSize );
	ParticleSystem::AddParticleSystem(partSys);

	// Add some water to the stage
	if(duck->GetDuckTexturePack() == 1)
	{
		result.m_increaseWaterLevel += 0.3f * duck->GetDuckSize();
	}
	else if(duck->GetDuckTexturePack() == 2)
	{
		result.m_decreaseWaterLevel += 0.6f * duck->GetDuckSize();
	}


	// Calculate left/right position for new ducks that we will create if DuckSize > 1
	glm::vec3 duckMoveDir = duck->GetMoveDir();
	glm::vec3 duckLeftPositionDir;
	glm::vec3 duckRightPositionDir;
	if(duck->GetDuckSize() > 1)
	{
		duckRightPositionDir = glm::cross(duckMoveDir, positiveZAxis);		
		duckLeftPositionDir = -duckRightPositionDir;				
	}
	float factorMove = duck->GetBoundingSphereRadius() * 2.0f;


	// Only "golden" ducks can spawn water draining ducks
	bool spawnWaterDrainingDuck = false;	
	if(duck->GetDuckTexturePack() == 3)
	{
		spawnWaterDrainingDuck = true;
	}
	
	// Spawn 3 new ducks?
	if(duck->GetDuckSize() == 3)
	{
		// Build a triangle of 3 new ducks
		std::vector<Duck* > newDucks;

		for(int i = 0; i < 3; i++)
		{
			Duck* newDuck = Duck::DuckPool::GetInstance().GetDuck();
			newDuck->SetSpawnWaterDrainingDuck(spawnWaterDrainingDuck);
			newDuck->SetScale(DUCKMEDIUM);
			newDuck->SetDuckSize(2);
			newDuck->SetRot(glm::vec3(90.0f, 0.0f, 0.0f));
			newDucks.push_back(newDuck);
		}				

		glm::vec4 duckMoveDirVec = glm::vec4(duckMoveDir.x, duckMoveDir.y, duckMoveDir.z, 0.0f);
		
		glm::vec4 transfDuckMoveDir = m_rotationSplitDuckLeft * duckMoveDirVec;
		glm::vec3 duckBottomLeftMoveDir = glm::vec3(transfDuckMoveDir.x, transfDuckMoveDir.y, transfDuckMoveDir.z);

		glm::vec4 transfDuckMoveDirSec = m_rotationSplitDuckRight * duckMoveDirVec;
		glm::vec3 duckBottomRightMoveDir = glm::vec3(transfDuckMoveDirSec.x, transfDuckMoveDirSec.y, transfDuckMoveDirSec.z);

		glm::vec3 topDuckPos = duck->GetPos() + duckMoveDir * factorMove;
		topDuckPos.z = DuckDepth(newDucks[0]->GetDuckSize(), gameDefData);
		newDucks[0]->SetPos(topDuckPos);
		newDucks[0]->SetMoveDir(duckMoveDir);

		glm::vec3 duckBottomLeftPos = duck->GetPos() + duckLeftPositionDir * factorMove;				
		duckBottomLeftPos.z = DuckDepth(newDucks[1]->GetDuckSize(), gameDefData);
		newDucks[1]->SetPos(duckBottomLeftPos);
		newDucks[1]->SetMoveDir(duckBottomLeftMoveDir);

		glm::vec3 duckBottomRightPos = duck->GetPos() + duckRightPositionDir * factorMove;
		duckBottomRightPos.z = DuckDepth(newDucks[2]->GetDuckSize(), gameDefData);
		newDucks[2]->SetPos(duckBottomRightPos);
		newDucks[2]->SetMoveDir(duckBottomRightMoveDir);

		for(int i = 0; i < 3; i++)
		{
			m_newDucks.push_back(newDucks[i]);			
		}
	}
	else if(duck->GetDuckSize() == 2) // Spawn 2 new ducks?
	{
		// Build 2 ducks facing left/right of original ducks facing direction
		Duck* duckLeft =Duck::DuckPool::GetInstance().GetDuck();
		duckLeft->SetSpawnWaterDrainingDuck(spawnWaterDrainingDuck);

		Duck* duckRight =Duck::DuckPool::GetInstance().GetDuck();
		duckRight->SetSpawnWaterDrainingDuck(spawnWaterDrainingDuck);

		duckLeft->SetScale(DUCKSMALL);
		duckRight->SetScale(DUCKSMALL);
		duckLeft->SetDuckSize(1);
		duckRight->SetDuckSize(1);

		duckLeft->SetRot(glm::vec3(90.0f, 0.0f, 0.0f));
		duckRight->SetRot(glm::vec3(90.0f, 0.0f, 0.0f));

		glm::vec3 duckLeftPos = duck->GetPos() + duckLeftPositionDir * factorMove;
		duckLeftPos.z = DuckDepth(duckLeft->GetDuckSize(), gameDefData);
		duckLeft->SetPos(duckLeftPos);
		duckLeft->SetMoveDir(duckLeftPositionDir);
		
		glm::vec3 duckRightPos = duck->GetPos() + duckRightPositionDir * factorMove;
		duckRightPos.z = DuckDepth(duckRight->GetDuckSize(), gameDefData);
		duckRight->SetPos(duckRightPos);
		duckRight->SetMoveDir(duckRightPositionDir);

		m_newDucks.push_back(duckLeft);
		m_newDucks.push_back(duckRight);
	}
}
//----------------------------------------------------------------------
void Duck::CollisionTick(std::vector<Duck* > &ducks, float deltaTime)
{
	for(auto &duck : ducks)
	{
		duck->TryMove(deltaTime); // Calculate next position
	}	
}
//----------------------------------------------------------------------
void Duck::Tick(std::vector<Duck* > &ducks, float deltaTime)
{	
	// Take care of potential new ducks created from the collision tick
	if(m_hasDestroyDucks)
	{
		int i = 0;
		while(i < ducks.size())
		{
			if(ducks[i]->GetDestroyed())
			{
				Duck::DuckPool::GetInstance().ReturnDuckToFreePool(ducks[i]);
				ducks.erase(ducks.begin() + i);
			}
			else
			{
				i++;
			}
		}
		m_hasDestroyDucks = false;
	}	
	for(auto &newDuck : m_newDucks)
	{
		// Check that none of the new ducks immiediatly collides with the world. If so, then dont add that duck.
		// It will be incredibly hard to cover and HANDLE every possible scenario in a nice way.
		// Not always spawning in the extra ducks is our way of handling it.		
		if(!DuckCollidesWithOtherDucks(newDuck, ducks) && DuckInsideWorldBorder(newDuck))
		{
			ducks.push_back(newDuck);
		}
		else
		{
			Duck::DuckPool::GetInstance().ReturnDuckToFreePool(newDuck);
		}
	}
	m_newDucks.clear();


	for(auto &duck : ducks)
	{
		duck->Tick(deltaTime);
		
		// Check if some duck managed to end up outside of the playing arena - if so, kill it
		if(!DuckInsideWorldBorder(duck))
		{
			duck->Destroy();
		}
	}
}
//----------------------------------------------------------------------
Duck::Duck()
{
	m_moveDir = glm::vec3(0.0f, 0.0f, 0.0f);
		
	m_pos = glm::vec3(0.0f, 0.0f, 0.0f);
	m_rot = glm::vec3(0.0f, 0.0f, 0.0f);

	m_collisionData = CollisionDataPool::GetInstance().GetCollisionData();

	Init();
}
//----------------------------------------------------------------------
void Duck::Init()
{
	m_moveSpeed = 0.9f + static_cast<float>(rand() % 3) * 0.1f;
	m_moveSpeedOrg = 0.0f;

	m_bounceBackFactor = 1.0f;
	m_bounceBack = glm::vec3(0.0f, 0.0f, 0.0f);
	m_bounceBackSpeed = 0.0f;
	m_bounceBackStartTime = -1.0f;

	m_startRotationAngle = 0.0f;
	m_targetRotationAngleDelta = 0.0f;
	m_rotationStarTime = -1.0f;
	m_rotationDurationTime = 0.35f;

	m_wobbleStartTime = -1.0f;
	m_wobbleCycleTime = 1.0f;
	m_wobbleAmount = 40.0f;

	m_collisionData->m_boundingSphereRadius = 0.1f; // Sphere radius should be 0.1 * scale

	m_autoDestroy = false;
	m_destroy = false;
	m_collided = false;

	int range = 4;
	m_duckTexturePack = rand() % range;
	if(m_duckTexturePack == 2)
	{
		m_duckTexturePack = 0;
	}
	
	m_duckDebugID = m_debugID;
	m_debugID++;	
}
//----------------------------------------------------------------------
Duck::~Duck()
{
	CollisionDataPool::GetInstance().ReturnCollisionDataToFreePool(m_collisionData);
}
//----------------------------------------------------------------------
void Duck::SetPos(glm::vec3 pos)
{
	m_pos = pos;
}
//----------------------------------------------------------------------
void Duck::SetRot(glm::vec3 rot)
{
	m_rot = rot;
}
//----------------------------------------------------------------------
void Duck::SetScale(float scale)
{
	m_scale = glm::vec3(scale, scale, scale);
	m_collisionData->m_boundingSphereRadius = 0.1f * scale;
}
//----------------------------------------------------------------------
void Duck::SetFacingRotation(float facingRotation)
{
	m_startRotationAngle = m_rot.y;
	m_targetRotationAngleDelta = facingRotation - m_rot.y;
	m_rotationStarTime = glfwGetTime();
}
//----------------------------------------------------------------------
void Duck::SetFacingRotationImmediate(float facingRotation)
{
	m_rot.y = facingRotation;

	// Model faces negative Y per default, so need to offset the rotation (0 degrees = facing -Y)
	float rotationWithOffset = m_rot.y - 90.0f;
	float rotationWithOffsetAsRadians = glm::radians(rotationWithOffset);
	m_moveDir = glm::vec3(cos(rotationWithOffsetAsRadians), sin(rotationWithOffsetAsRadians), 0.0f);	
}
//----------------------------------------------------------------------
void Duck::SetMoveDir(glm::vec3 dir)
{
	m_moveDir = glm::normalize(dir);

	// Auto rotate around y axis to face direction we move in
	const glm::vec3 defaultFacingDir(0.0f, -1.0f, 0.0f);
	m_rot.y = acos(glm::dot(defaultFacingDir, m_moveDir)) * 180.0 / M_PI;

	if(dir.x < 0.0f)
	{
		m_rot.y = 360.0f - m_rot.y;
	}

	#ifdef DEBUG
		m_logDebug << m_rot.y  << endl;
	#endif
}
//----------------------------------------------------------------------
void Duck::SetDuckSize(unsigned int duckSize)
{
	m_duckSize = duckSize;
}
//----------------------------------------------------------------------
void Duck::SetMoveSpeed(float speed)
{
	#ifdef DEBUG
		//m_logDebug << "SetMoveSpeed: " << speed << endl;
	#endif
	m_moveSpeed = speed;
}
//----------------------------------------------------------------------
void Duck::SetSpawnWaterDrainingDuck(bool spawnWaterDrainingDuck)
{
	if(spawnWaterDrainingDuck)
	{
		m_duckTexturePack = 2;
		m_autoDestroy = true;
		m_autoDestroyStartTime = glfwGetTime();
		m_autoDestroyLifeTime = 2.0f; // Autodestroying ducks live for this many seconds
	}
	else
	{
		int range = 4;
		m_duckTexturePack = rand() % range;
		if(m_duckTexturePack == 2)
		{
			m_duckTexturePack = 0;
		}
	}	
}
//----------------------------------------------------------------------
glm::vec3& Duck::GetPos()
{
	return m_pos;
}
//----------------------------------------------------------------------
glm::vec3& Duck::GetPosTry()
{
	return m_collisionData->m_posTry;
}
//----------------------------------------------------------------------
glm::vec3&	Duck::GetMoveDir()
{
	return m_moveDir;
}
//----------------------------------------------------------------------
float& Duck::GetBoundingSphereRadius()
{
	return m_collisionData->m_boundingSphereRadius;
}
//----------------------------------------------------------------------
float& Duck::GetMoveSpeed()
{
	return m_moveSpeed;
}
//----------------------------------------------------------------------
float& Duck::GetMoveSpeedOrg()
{
	return m_moveSpeedOrg;
}
//----------------------------------------------------------------------
float& Duck::GetFacingRotation()
{
	return m_rot.y;
}
//----------------------------------------------------------------------
unsigned int& Duck::GetDuckSize()
{
	return m_duckSize;
}
//----------------------------------------------------------------------
bool Duck::GetDestroyed()
{
	return m_destroy;
}
//----------------------------------------------------------------------
unsigned char Duck::GetCollisionDataIndex() const
{
	return m_collisionData->m_collsionPoolIndex;
}
//----------------------------------------------------------------------
int Duck::GetDuckTexturePack() const
{
	return m_duckTexturePack;
}
//----------------------------------------------------------------------
int Duck::GetDebugId() const
{
	return m_duckDebugID;
}
//----------------------------------------------------------------------
void Duck::RememberSpeed()
{
	m_moveSpeedOrg = m_moveSpeed;

	#ifdef DEBUG
		m_logDebug << m_duckDebugID <<  "RememberSpeed, m_moveSpeedOrg: " << m_moveSpeedOrg << endl;
	#endif	
}
//----------------------------------------------------------------------
void Duck::ResetSpeed()
{
	m_moveSpeed = m_moveSpeedOrg;

	#ifdef DEBUG
		m_logDebug << m_duckDebugID <<  "ResetSpeed, m_moveSpeed: " << m_moveSpeed << endl;
	#endif
	
}
//----------------------------------------------------------------------
void Duck::BounceBack()
{
	if(!BounceBackActive())
	{
		m_bounceBack = -m_moveDir;
		RememberSpeed();
		SetMoveSpeed(0.0f);
		StartBounceBack();

		#ifdef DEBUG
			m_logDebug << m_duckDebugID << ": m_bounceBack: " << m_bounceBack.x << ", " << m_bounceBack.y << ", " << m_bounceBack.z <<  endl;
		#endif
	}
}
//----------------------------------------------------------------------
void Duck::Divert(float divertAngle)
{
	SetFacingRotation(m_rot.y + divertAngle);
}
//----------------------------------------------------------------------
void Duck::TryMove(float deltaTime)
{
	DoRotation();
	m_collisionData->m_posTry = m_pos;

	DoBounceBack(); // Calculate m_bounceBackSpeed
	DoMove(deltaTime, m_collisionData->m_posTry);
}
//----------------------------------------------------------------------
bool Duck::BounceBackActive() const
{
	return m_bounceBackStartTime > 0.0f;
}
//----------------------------------------------------------------------
void Duck::StartBounceBack()
{
	if(!BounceBackActive())
	{
		m_bounceBackStartTime = float(glfwGetTime());
	}
}
//----------------------------------------------------------------------
void Duck::DoBounceBack()
{
	if(BounceBackActive())
	{
		float deltaTime = float(glfwGetTime()) - m_bounceBackStartTime;

		#ifdef DEBUG
			m_logDebug << m_duckDebugID << "DoBounceBack::deltaTime:" << deltaTime << endl;
		#endif
		

		// Slow down until we reach 1.5 secs, where bounce back should be complete
		float bounceBackTotalTime = 1.5f;
		float slowDownFactor = bounceBackTotalTime - deltaTime;

		if(slowDownFactor <= 0.0f)
		{
			m_bounceBackStartTime = -1.0f;
			m_bounceBackSpeed = 0.0f;
			ResetSpeed();

			#ifdef DEBUG
				m_logDebug << m_duckDebugID <<  "DoBounceBack finished " << endl;
			#endif
		}
		else
		{
			m_bounceBackSpeed = (m_moveSpeedOrg * m_bounceBackFactor) * slowDownFactor / bounceBackTotalTime;
			
			#ifdef DEBUG				
				m_logDebug << m_duckDebugID << "DoBounceBack slowDownFactor: " << slowDownFactor << endl;
				m_logDebug << m_duckDebugID << "DoBounceBack m_bounceBackSpeed: " << m_bounceBackSpeed << endl;
			#endif
		}
	}
}
//----------------------------------------------------------------------
void Duck::DoRotation()
{
	if(m_rotationStarTime > 0.0f)
	{
		float deltaTime = float(glfwGetTime()) - m_rotationStarTime;

		if(deltaTime > m_rotationDurationTime)
		{
			m_rotationStarTime = -1.0f;
		}
		else
		{
			float t = deltaTime / m_rotationDurationTime;

			m_rot.y = m_startRotationAngle + t * m_targetRotationAngleDelta;

			// Model faces negative Y per default, so need to offset the rotation (0 degrees = facing -Y)
			const float rotationWithOffset = m_rot.y -90.0f;
			m_moveDir = glm::vec3(cos(glm::radians(rotationWithOffset)), sin(glm::radians(rotationWithOffset)), 0.0f);			
		}
	}
}
//----------------------------------------------------------------------
void Duck::DoWobble()
{
	if( m_wobbleStartTime < 0.0f)
	{
		m_wobbleStartTime = float(glfwGetTime());
	}

	float deltaTime = float(glfwGetTime()) - m_wobbleStartTime;

	if(deltaTime > m_wobbleCycleTime)
	{
		m_wobbleStartTime = -1.0f;
	}
	else
	{
		float t = deltaTime / m_wobbleCycleTime;

		if(t > 0.5f)
		{
			m_rot.z = (m_wobbleAmount * 0.5f) - (t - 0.5f) * m_wobbleAmount;
		}
		else
		{
			m_rot.z = t * m_wobbleAmount;
		}
	}
}
//----------------------------------------------------------------------
void Duck::Destroy()
{
	m_destroy = true;
}
//----------------------------------------------------------------------
void Duck::Collided()
{
	m_collided = true;
}
//----------------------------------------------------------------------
void Duck::Tick(float deltaTime)
{
	DoRotation(); // Update rotation facing direction
	DoWobble(); // Update rotation around center point ("wobble effect")

	/*
	DoBounceBack(); // Calculates m_bounceBackSpeed, if bounce back has been initiated	

	if(!m_collided)
	{
		DoMove(deltaTime, m_pos); // Update position
	}

	DoAutoDestroy();
	*/

	m_collided = false;
}
//----------------------------------------------------------------------
void Duck::DoAutoDestroy()
{
	if(m_autoDestroy && !m_destroy)
	{
		float timeDiff = glfwGetTime() - m_autoDestroyStartTime;
		if(timeDiff > m_autoDestroyLifeTime)
		{
			DuckDestroyResult result; // We do not care about the result of an autodestroyed duck - nothing is going to be change after this
			Duck::Destroy(this, m_gameDefData, result);
		}
	}
}
//----------------------------------------------------------------------
void Duck::DoMove(float deltaTime, glm::vec3 &pos)
{
	float moveSpeedTimeAdjusted = m_moveSpeed * deltaTime;
	float bounceBackSpeedTimeAdjusted = m_bounceBackSpeed * deltaTime;

	#ifdef DEBUG
		m_logDebug << m_duckDebugID << "moveSpeedTimeAdjusted: " << moveSpeedTimeAdjusted << ", m_moveSpeed: " << m_moveSpeed <<  endl;
		m_logDebug << m_duckDebugID << "bounceBackSpeedTimeAdjusted: " << bounceBackSpeedTimeAdjusted << ", m_bounceBackSpeed: " << m_bounceBackSpeed <<  endl;
	#endif

	pos += (m_moveDir * moveSpeedTimeAdjusted) + (m_bounceBack * bounceBackSpeedTimeAdjusted);

	DoUpdateWaterLevel(pos);
}
//----------------------------------------------------------------------
void Duck::DoUpdateWaterLevel(glm::vec3 &pos)
{
	pos.z = DuckDepth(GetDuckSize(), m_gameDefData);
}
//----------------------------------------------------------------------
void Duck::Render(const RenderStateModel3D& renderState, const glm::mat4 &projection, const glm::mat4 &view)
{
	#ifdef USE_PBR
		// No need for the fancy different textures, this one looks the best so lets use it for the multiplayer version for now
		m_modelCookTorrance->SetTexture(GetTextureResource(DUCK_PLASTIC_ALBEDO_TEXTURE),	GetTextureResource(DUCK_PLASTIC_NORMAL_TEXTURE),
										GetTextureResource(DUCK_PLASTIC_METALLIC_TEXTURE),	GetTextureResource(DUCK_PLASTIC_ROUGHNESS_TEXTURE),
										GetTextureResource(DUCK_PLASTIC_AO_TEXTURE));

		m_modelCookTorrance->Render(renderState, projection, view, m_pos, m_rot, m_scale);	
	#else
		m_model->Render(renderState, projection, view, m_pos, m_rot, m_scale);
	#endif

}


//----------------------------------------------------------------------
//----------------------------------------------------------------------

Duck::DuckPool& Duck::DuckPool::GetInstance()
{
	static DuckPool s_particlePool = DuckPool();
	return s_particlePool;
}
//----------------------------------------------------------------------
Duck::DuckPool::DuckPool()
{
	// Create one big continuous memory section
	m_ducks.resize(MAX_NR_DUCKS);

	// Initially, all particles are free so lets link them all together
	for(int i = 0; i < MAX_NR_DUCKS - 1; i++)
	{
		m_ducks[i].m_nextFree =	&m_ducks[i + 1];
	}
	m_ducks[MAX_NR_DUCKS - 1].m_nextFree = NULL;

	m_firstFree = &m_ducks[0];
}
//----------------------------------------------------------------------
Duck* Duck::DuckPool::GetDuck()
{
	Duck* duck = NULL;
	if(m_firstFree)
	{
		duck = m_firstFree;
		m_firstFree = m_firstFree->m_nextFree;
	}

	if(duck)
	{
		duck->Init();
	}
	return duck;
}
//----------------------------------------------------------------------
void Duck::DuckPool::ReturnDuckToFreePool(Duck* p)
{
	p->m_nextFree = m_firstFree;
	m_firstFree = p;
}
//----------------------------------------------------------------------
Duck::DuckPool::~DuckPool()
{
}
//----------------------------------------------------------------------

//----------------------------------------------------------------------
//----------------------------------------------------------------------
#define MAX_NR_COLLISIONDATA 200

CollisionDataPool& CollisionDataPool::GetInstance()
{
	static CollisionDataPool s_collisionDataPool = CollisionDataPool();
	return s_collisionDataPool;
}
//----------------------------------------------------------------------
CollisionDataPool::CollisionDataPool()
{
	// Create one big continuous memory section
	m_collisionDatas.resize(MAX_NR_COLLISIONDATA);

	// Initially, all are free so lets link them all together
	for(int i = 0; i < MAX_NR_COLLISIONDATA - 1; i++)
	{
		m_collisionDatas[i].m_nextFree = &m_collisionDatas[i + 1];
		m_collisionDatas[i].m_collsionPoolIndex = static_cast<unsigned char>(i);
	}
	m_collisionDatas[MAX_NR_COLLISIONDATA - 1].m_nextFree = NULL;
	m_collisionDatas[MAX_NR_COLLISIONDATA - 1].m_collsionPoolIndex = MAX_NR_COLLISIONDATA - 1;

	m_firstFree = &m_collisionDatas[0];
}
//----------------------------------------------------------------------
CollisionData* CollisionDataPool::GetCollisionData()
{
	CollisionData* collisionData = NULL;
	if(m_firstFree)
	{
		collisionData = m_firstFree;
		m_firstFree = m_firstFree->m_nextFree;
	}

	return collisionData;
}
//----------------------------------------------------------------------
const CollisionData& CollisionDataPool::GetCollsionData(unsigned int index) const
{
	return m_collisionDatas[index];
}
//----------------------------------------------------------------------
void CollisionDataPool::ReturnCollisionDataToFreePool(CollisionData* p)
{
	p->m_nextFree = m_firstFree;
	m_firstFree = p;
}
//----------------------------------------------------------------------
CollisionDataPool::~CollisionDataPool()
{
}