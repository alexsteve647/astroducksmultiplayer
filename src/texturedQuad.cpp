#include <iostream>
using namespace std;

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "texturedQuad.h"
#include "shader.h"

// Statics
GLuint TexturedQuad::m_programID;
GLuint TexturedQuad::m_mvpID;
GLuint TexturedQuad::m_textureID;


void TexturedQuad::InitTexturedQuad()
{
	m_programID = LoadShaders( "texturedQuad.vertexshader", "texturedQuad.fragmentshader" );	

	m_mvpID = glGetUniformLocation(m_programID, "MVP");
	m_textureID  = glGetUniformLocation(m_programID, "myTextureSampler");
}
//----------------------------------------------------------------------
void TexturedQuad::Tick(std::vector<std::shared_ptr<TexturedQuad> > &animatedRectangles)
{
	for(auto animatedRectangle : animatedRectangles)
	{
		animatedRectangle->Tick();
	}
}
//----------------------------------------------------------------------
TexturedQuad::TexturedQuad(glm::vec3 start, float width, float height, GLuint texture, float rotation)
{
	m_textures.push_back(texture);

	m_start = start;

	m_width = width;
	m_height = height;

	m_hasRotation = false;
	/*
	if(rotation > 0.001f || rotation < 0.001f)
	{
		m_hasRotation = true;
		m_rotation = rotation;
	}
	*/

	CreateRectanglePositionData();
	CreateRectangleUVData();
	BuildVBOS();
	
	m_speedAnimation = 0.0f;
	m_currentFrame = 0;
	m_animTime = 0.0f;
	m_firstAnimTime = true;
	m_playReverse = false;	
}
//----------------------------------------------------------------------
TexturedQuad::TexturedQuad(glm::vec3 start, float width, float height, 
										std::vector<GLuint> textures, float speedAnimation)
{
	for(auto textureId : textures)
	{
		m_textures.push_back(textureId);
	}
	
	m_start = start;

	m_width = width;
	m_height = height;

	m_hasRotation = false;
	m_rotation = 0.0f;	

	CreateRectanglePositionData();
	CreateRectangleUVData();
	BuildVBOS();
	
	m_speedAnimation = speedAnimation;
	m_currentFrame = 0;
	m_animTime = 0.0f;
	m_firstAnimTime = true;
	m_playReverse = false;
}
//----------------------------------------------------------------------
void TexturedQuad::CreateRectanglePositionData()
{
	/*
		----> X
		|
		|
		|
		Y

		P1     P2

		P4     P3
	*/

	glm::vec3 P1 = glm::vec3(m_start.x - m_width / 2.0f, m_start.y - m_height / 2.0f, m_start.z);
	glm::vec3 P2 = glm::vec3(m_start.x + m_width / 2.0f, m_start.y - m_height / 2.0f, m_start.z);
	glm::vec3 P3 = glm::vec3(m_start.x + m_width / 2.0f, m_start.y + m_height / 2.0f, m_start.z);
	glm::vec3 P4 = glm::vec3(m_start.x - m_width / 2.0f, m_start.y + m_height / 2.0f, m_start.z);

	// Apply rotation to the triangle we built
	if(m_hasRotation)
	{
		// Create a rotational matrix around the Z axis and rotate it with m_rotation.
		glm::mat4 rotate = glm::mat4(1.0f);
		rotate = glm::rotate(rotate, m_rotation, glm::vec3(0.0f, 0.0f, 1.0f));

		glm::vec4 P1_w = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
		glm::vec4 P2_w = glm::vec4(m_width, 0, 0.0f, 1.0f);
		glm::vec4 P3_w = glm::vec4(m_width, m_height, 0.0f, 1.0f);
		glm::vec4 P4_w = glm::vec4(0.0f, m_height, 0.0f, 1.0f);
				
		P1_w = rotate * P1_w;
		P2_w = rotate * P2_w;
		P3_w = rotate * P3_w;
		P4_w = rotate * P4_w;

		P1 = glm::vec3(m_start.x + P1_w.x, m_start.y + P1_w.y, 0.0f);
		P2 = glm::vec3(m_start.x + P2_w.x, m_start.y + P2_w.y, 0.0f);
		P3 = glm::vec3(m_start.x + P3_w.x, m_start.y + P3_w.y, 0.0f);
		P4 = glm::vec3(m_start.x + P4_w.x, m_start.y + P4_w.y, 0.0f);
	}	

	m_vertices.clear();
	m_vertices.push_back(P3);
	m_vertices.push_back(P2);
	m_vertices.push_back(P1);

	m_vertices.push_back(P1);
	m_vertices.push_back(P4);
	m_vertices.push_back(P3);
}
//----------------------------------------------------------------------
void TexturedQuad::CreateRectangleUVData()
{
	m_verticesUV.clear();
	glm::vec2 UV1(1.0f, 0.0f);
	glm::vec2 UV2(1.0f, 1.0f);
	glm::vec2 UV3(0.0f, 1.0f);
	m_verticesUV.push_back(UV1);
	m_verticesUV.push_back(UV2);
	m_verticesUV.push_back(UV3);

	glm::vec2 UV4(0.0f, 1.0f);
	glm::vec2 UV5(0.0f, 0.0f);
	glm::vec2 UV6(1.0f, 0.0f);
	m_verticesUV.push_back(UV4);
	m_verticesUV.push_back(UV5);
	m_verticesUV.push_back(UV6);	
}
//----------------------------------------------------------------------
void TexturedQuad::BuildVBOS()
{
	glGenBuffers(1, &m_verticesVBO);
	glGenBuffers(1, &m_verticesUVVBO);

	glBindBuffer(GL_ARRAY_BUFFER, m_verticesVBO);
	glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(glm::vec3), &m_vertices[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_verticesUVVBO);
	glBufferData(GL_ARRAY_BUFFER, m_vertices.size() * sizeof(glm::vec2), &m_verticesUV[0], GL_STATIC_DRAW);		
}
//----------------------------------------------------------------------
void TexturedQuad::PlayReverse(bool reverse)
{
	m_playReverse = reverse;

	if(m_playReverse)
	{
		m_currentFrame = m_textures.size() - 1;
	}
	else
	{
		m_currentFrame = 0;
	}
}
//----------------------------------------------------------------------
bool TexturedQuad::GetPlayReverse() const
{
	return m_playReverse;
}
//----------------------------------------------------------------------
void TexturedQuad::Tick()
{
	if(m_firstAnimTime)
	{
		m_animTime = glfwGetTime();
		m_firstAnimTime = false;
	}
	else
	{
		double currentTime = glfwGetTime();

		if(currentTime - m_animTime > m_speedAnimation)
		{			
			m_animTime = currentTime;
			if(GetPlayReverse())
			{
				m_currentFrame--;
				if(m_currentFrame <= 0)
				{
					m_currentFrame = m_textures.size() - 1;
				}
			}
			else
			{
				m_currentFrame++;
				if(m_currentFrame >= m_textures.size())
				{
					m_currentFrame = 0;
				}
			}
		}
	}
}
//----------------------------------------------------------------------
void TexturedQuad::Render(const glm::mat4 &projection, const glm::mat4 &view)
{
	glUseProgram(m_programID);
	glEnable(GL_BLEND);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glm::mat4 vp = projection * view;
	glUniformMatrix4fv(m_mvpID, 1, GL_FALSE, &vp[0][0]);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_verticesVBO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, m_verticesUVVBO);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glActiveTexture(GL_TEXTURE0);		
	glBindTexture(GL_TEXTURE_2D, Texture());
	
	// Set "myTextureSampler" sampler to use Texture Unit 0
	glUniform1i(m_textureID, 0);			
	glDrawArrays(GL_TRIANGLES, 0, m_vertices.size());

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisable(GL_BLEND);
	
	glEnable(GL_DEPTH_TEST);
}
//----------------------------------------------------------------------
GLuint TexturedQuad::Texture() const
{
	return m_textures[m_currentFrame];
}
//----------------------------------------------------------------------
TexturedQuad::~TexturedQuad()
{
	glDeleteBuffers(1, &m_verticesVBO);
	glDeleteBuffers(1, &m_verticesUVVBO);	
}