#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include <map>
#include "texture.h"
using namespace std;

const char * pathDelim();

map<string, GLuint> g_textureLookup;

#define LINUX_DEF

#ifdef LINUX_DEF
const char* pathDelim()
{
	return "/";
}
#endif

#ifndef LINUX_DEF
const char* pathDelim()
{
	return "\\";
}
#endif


#define TEXTUREPATH "gfx"
#define WATERPATH "water"
#define HUDPATH "hud"
#define TEXTURES_PATH "textures"


GLuint loadBMP_custom(const char * imagepath, bool createMipmap, bool sRGB)
{
	std::map<string, GLuint>::iterator it = g_textureLookup.find(string(imagepath));
	if(it != g_textureLookup.end())
	{
		//cout << "Getting texture from lookup" << endl;
		return it->second;
	}

	//printf("Reading image %s\n", imagepath);	

	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	unsigned int width, height;
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file = fopen(imagepath,"rb");
	if (!file)
	{
		printf("%s could not be opened. Are you in the right directory ? Don't forget to read the FAQ !\n", imagepath);
		getchar();
		return 0;
	}

	// Read the header, i.e. the 54 first bytes

	// If less than 54 bytes are read, problem
	if ( fread(header, 1, 54, file)!=54 ){ 
		printf("Not a correct BMP file\n");
		fclose(file);
		return 0;
	}
	// A BMP files always begins with "BM"
	if ( header[0]!='B' || header[1]!='M' ){
		printf("Not a correct BMP file\n");
		fclose(file);
		return 0;
	}
	// Make sure this is a 24bpp file
	if ( *(int*)&(header[0x1E])!=0  )         {printf("Not a correct BMP file\n");    fclose(file); return 0;}
	if ( *(int*)&(header[0x1C])!=24 )         {printf("Not a correct BMP file\n");    fclose(file); return 0;}

	// Read the information about the image
	dataPos    = *(int*)&(header[0x0A]);
	imageSize  = *(int*)&(header[0x22]);
	width      = *(int*)&(header[0x12]);
	height     = *(int*)&(header[0x16]);

	// Some BMP files are misformatted, guess missing information
	if (imageSize==0)    imageSize=width*height*3; // 3 : one byte for each Red, Green and Blue component
	if (dataPos==0)      dataPos=54; // The BMP header is done that way

	// Create a buffer
	data = new unsigned char [imageSize];

	// Read the actual data from the file into the buffer
	int size = fread(data,1,imageSize,file);

	// Everything is in memory now, the file can be closed.
	fclose (file);

	// Create one OpenGL texture
	GLuint textureID;
	glGenTextures(1, &textureID);
	
	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);

	// Give the image to OpenGL
	if(sRGB) // Means we will use gamma correction as well
	{
		glTexImage2D(GL_TEXTURE_2D, 0,GL_SRGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);
	}
	else
	{
		glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);
	}	

	// OpenGL has now copied the data. Free our own version
	delete [] data;

	// Poor filtering, or ...
	if(!createMipmap)
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); 
	}
	else
	{
		// ... nice trilinear filtering ...
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		// ... which requires mipmaps. Generate them automatically.
		glGenerateMipmap(GL_TEXTURE_2D);
	}


	g_textureLookup.insert (pair<string, GLuint>(std::string(imagepath), textureID));

	// Return the ID of the texture we just created
	return textureID;
}

GLuint GetTextureResource(GameTextureResource res, bool createMipmap)
{
	string path = TEXTUREPATH;

	bool sRGB = false;

	if(createMipmap)
	{
		if(res == DUCK_PLASTIC_ALBEDO_TEXTURE || res == DUCK_WOOD_ALBEDO_TEXTURE || res == DUCK_METAL_ALBEDO_TEXTURE ||
			res == DUCK_GOLD_ALBEDO_TEXTURE ||  res == FISHINGBOAT_ALBEDO_TEXTURE || res == PROJECTILE_BALL_ALBEDO_TEXTURE)
		{
			sRGB = true;
		}
	}

	if(res >= HUD_PLAYER_HEART_FULL && res <= HUD_GFX_END)
	{
		path += string(pathDelim()) + HUDPATH + string(pathDelim());
		switch(res)
		{
			case HUD_PLAYER_HEART_FULL:
			path += "heart";
			break;

			case HUD_TEXT2D_FONT:
			path += "font_ub_mono_alphadig";
			break;

			case HUD_TEXT2D_COLON:
			path += "colon";
			break;
		}
	}
	else if(res >= HUD_GFX_END)
	{
		path += string(pathDelim()) + TEXTURES_PATH + string(pathDelim());
		switch(res)
		{		
			case DUCK_PLASTIC_ALBEDO_TEXTURE:
			path += "duck_plastic_alb";
			break;

			case DUCK_PLASTIC_AO_TEXTURE:
			path += "duck_plastic_alb";
			break;

			case DUCK_PLASTIC_ROUGHNESS_TEXTURE:
			path += "duck_plastic_rgh";
			break;

			case DUCK_PLASTIC_METALLIC_TEXTURE:
			path += "duck_plastic_mtl";
			break;

			case DUCK_PLASTIC_NORMAL_TEXTURE:
			path += "duck_plastic_nrm";
			break;

			case DUCK_WOOD_ALBEDO_TEXTURE:
			path += "duck_wood_alb";
			break;

			case DUCK_WOOD_AO_TEXTURE:
			path += "duck_wood_alb";
			break;

			case DUCK_WOOD_ROUGHNESS_TEXTURE:
			path += "duck_wood_rgh";
			break;

			case DUCK_WOOD_METALLIC_TEXTURE:
			path += "duck_wood_mtl";
			break;

			case DUCK_WOOD_NORMAL_TEXTURE:
			path += "duck_wood_nrm";
			break;

			case DUCK_METAL_ALBEDO_TEXTURE:
			path += "duck_metal_alb";
			break;

			case DUCK_METAL_AO_TEXTURE:
			path += "duck_metal_alb";
			break;

			case DUCK_METAL_ROUGHNESS_TEXTURE:
			path += "duck_metal_rgh";
			break;

			case DUCK_METAL_METALLIC_TEXTURE:
			path += "duck_metal_mtl";
			break;

			case DUCK_METAL_NORMAL_TEXTURE:
			path += "duck_metal_nrm";
			break;

			case DUCK_GOLD_ALBEDO_TEXTURE:
			path += "duck_gold_alb";
			break;

			case DUCK_GOLD_AO_TEXTURE:
			path += "duck_gold_alb";
			break;

			case DUCK_GOLD_ROUGHNESS_TEXTURE:
			path += "duck_gold_rgh";
			break;

			case DUCK_GOLD_METALLIC_TEXTURE:
			path += "duck_gold_mtl";
			break;

			case DUCK_GOLD_NORMAL_TEXTURE:
			path += "duck_gold_nrm";
			break;

			case FISHINGBOAT_ALBEDO_TEXTURE:
			path += "Fishing boat_albedo";
			break;

			case FISHINGBOAT_AO_TEXTURE:
			path += "Fishing boat_ao";
			break;

			case FISHINGBOAT_ROUGHNESS_TEXTURE:
			path += "Fishing boat_roughness";
			break;

			case FISHINGBOAT_METALLIC_TEXTURE:
			path += "Fishing boat_metallic";
			break;

			case FISHINGBOAT_NORMAL_TEXTURE:
			path += "Fishing boat_normal";
			break;

			case FISHINGBOAT_PLAYER_TWO_ALBEDO_TEXTURE:		
			path += "Fishing boat_albedoPlayerTwo";
			break;

			case PROJECTILE_BALL_ALBEDO_WHITE_TEXTURE:
			path += "cannonBall_albedo_white";
			break;

			case PROJECTILE_BALL_ALBEDO_TEXTURE:
			path += "cannonBall_albedo";
			break;

			case PROJECTILE_BALL_AO_TEXTURE:
			path += "cannonBall_albedo";
			break;

			case PROJECTILE_BALL_ROUGHNESS_TEXTURE:
			path += "cannonBall_rgh";
			break;

			case PROJECTILE_BALL_METALLIC_TEXTURE:
			path += "cannonBall_mtl";
			break;

			case PROJECTILE_BALL_NORMAL_TEXTURE:
			path += "cannonBall_nrm";
			break;

			case POOL_WALLS_TEXTURE:
			path += "tiles_albedo";
			break;

			case POOL_WALLS_BOTTOM:
			path += "tiles_albedo_drain";
			break;

		}		
	}

	path += ".bmp";
	return loadBMP_custom(path.c_str(), createMipmap, sRGB);		
}

void ClearTextureCache()
{
	for(auto texture : g_textureLookup)
	{
		glDeleteTextures(1, &texture.second);
	}
	
	g_textureLookup.clear();
}