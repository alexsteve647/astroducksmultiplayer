#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "collisionSystem.h"
#include "collisionDetection.h"
#include "quadTree.h"
#include "duck.h"
#include "player.h"
#include "projectileBall.h"
#include "gameDefs.h"

#include <iostream>
using namespace std;

#define USE_QUAD_TREE
//#define DEBUG
//#define DEBUG_PRINT_DUCK_COLLISION_TEST

CollisionEngine& CollisionEngine::GetInstance(std::vector<Duck* > &ducks, std::vector<std::unique_ptr<Player> > &players)
{
	static CollisionEngine s_engine = CollisionEngine(ducks, players);
	return s_engine;
}
//----------------------------------------------------------------------
//----------------------------------------------------------------------
CollisionEngine::CollisionEngine(std::vector<Duck* > &ducks, std::vector<std::unique_ptr<Player> > &players) :
	m_ducks(ducks), m_players(players), m_quadTree(0)
{
	m_xMin = WORLD_XMIN;
	m_xMax = WORLD_XMAX;
	m_yMin = WORLD_YMIN;
	m_yMax = WORLD_YMAX;
	m_quadTree.SetWorldSize(m_xMin, m_yMin, m_xMax, m_yMax);
}
//----------------------------------------------------------------------
CollisionEngine::~CollisionEngine()
{

}
//----------------------------------------------------------------------
void CollisionEngine::HandleProjectileCollision(const GameDefData* gameDefData, CollisionResult &result)
{
	// Check for collision with projectiles	and player	
	int j = 0;	
	while(j < m_ducks.size())
	{
		Duck* duck = m_ducks[j];
		bool destroyed = false;

		// Check collision with players
		for (auto &player : m_players)
		{
			destroyed = SphereSphereTest(duck->GetPosTry(), duck->GetBoundingSphereRadius(), player->GetPosTry(), 
							player->GetBoundingSphereRadius());
								
			// collided with the players boat, apply damage to player
			if(destroyed)
			{
				player->TakeDamage(1);
			}

			// Check projectiles of the player			
			std::vector<std::unique_ptr<ProjectileBall> > &projectiles = player->GetProjectileBalls();
			int projectilesSize = projectiles.size();			
			int projectileCounter = -1;
			if(!destroyed && projectilesSize > 0)
			{
				projectileCounter = 0;
				while(projectileCounter < projectilesSize && !destroyed)
				{
					ProjectileBall* proj = projectiles[projectileCounter].get();
					destroyed = SphereSphereTest(duck->GetPosTry(), duck->GetBoundingSphereRadius(), proj->GetPos(), 
													proj->GetBoundingSphereRadius());
					if(!destroyed)
					{
						projectileCounter++;
					}
				}
			}

			/* If duck got hit, remove the projectile and the duck,
				then spawn new ducks depending on the ducks size
			*/
			if(destroyed)
			{
				DuckDestroyResult duckResult;
				Duck::Destroy(duck, gameDefData, duckResult);

				result.m_increaseWaterLevel += duckResult.m_increaseWaterLevel;				
				result.m_decreaseWaterLevel += duckResult.m_decreaseWaterLevel;
				
				if(projectileCounter >= 0)
				{					
					player->DestroyProjectile(projectileCounter);
					
					// Award player with score
					player->IncreaseScore(10);
				}
			}
		}
		j++;
	}

}
//----------------------------------------------------------------------
void CollisionEngine::HandlePlayerCollideWithWorld()
{
	for (auto &player : m_players)
	{
		bool collidedwithWorld = SphereBorderTest(player->GetPosTry(), player->GetBoundingSphereRadius(), m_xMin, m_yMin, m_xMax, m_yMax);
		if(collidedwithWorld)
		{
			player->HitWorldBorder();
		}
	}	
}
//----------------------------------------------------------------------
void CollisionEngine::Tick(const GameDefData* gameDefData, CollisionResult &result)
{
	HandleProjectileCollision(gameDefData, result);

	HandlePlayerCollideWithWorld();

	#ifdef USE_QUAD_TREE
		// Insert all ducks into the quad tree
		m_quadTree.Clear();	
		for(auto &duck : m_ducks)
		{
			m_quadTree.Insert(duck);
		}
	#endif


	//m_quadTree.PrintTree();

	std::vector<Duck* > collidables;
	collidables.reserve(10);
	for(auto &duck : m_ducks)
	{
		unsigned int i = 0;		
		unsigned int nrCollidables = 0;
		collidables.clear();

		#ifdef USE_QUAD_TREE
			m_quadTree.GetCollidables(duck, collidables);
			nrCollidables = collidables.size();
		#else
			nrCollidables = m_ducks.size();
		#endif

		unsigned char	duckCollisionDataIndex = duck->GetCollisionDataIndex();
		bool			duckCollidedWithOtherDuck = false;
		unsigned char	collidableDucksIndices[MAX_NR_DUCKS];
		for(auto &collidableDuck : collidables)
		{
			collidableDucksIndices[i] = collidableDuck->GetCollisionDataIndex();
			i++;
		}
		

		#ifdef DEBUG_PRINT_DUCK_COLLISION_TEST
		if(nrCollidables > 25) // Not interested in seeing anything here until we got some more ducks
		{
			cout << "******** DEBUG COLLISION TEST DUCK -> DUCK ********" << endl;
			cout << "nrCollidables : " << nrCollidables << endl;
			cout << "CollisionData index: " << static_cast<unsigned int>(duckCollisionDataIndex) << endl;
			for(int dbgc = 0; dbgc < nrCollidables; dbgc ++)
			{
				cout << "CollisionData index: " << static_cast<unsigned int>(collidableDucksIndices[dbgc]) << endl;
			}
			cout << "******** DEBUG COLLISION TEST DUCK -> DUCK ********" << endl;
		}
		#endif

		i = 0;
		while(i < nrCollidables && !duckCollidedWithOtherDuck)
		{
			if(collidableDucksIndices[i] != duckCollisionDataIndex)
			{
				duckCollidedWithOtherDuck = SphereSphereTest(CollisionDataPool::GetInstance().GetCollsionData(static_cast<unsigned int>(duckCollisionDataIndex)).m_posTry,
															CollisionDataPool::GetInstance().GetCollsionData(static_cast<unsigned int>(duckCollisionDataIndex)).m_boundingSphereRadius,
															CollisionDataPool::GetInstance().GetCollsionData(static_cast<unsigned int>(collidableDucksIndices[i])).m_posTry,
															CollisionDataPool::GetInstance().GetCollsionData(static_cast<unsigned int>(collidableDucksIndices[i])).m_boundingSphereRadius);
			}														
			i++;														
		}

		if(duckCollidedWithOtherDuck)
		{
			duck->Collided();
			float deflectAngle = 10.0f + static_cast<float>(rand() % 10);
			duck->Divert(deflectAngle);			
			duck->BounceBack();
		}
		else
		{
			bool collidedwithWorld = SphereBorderTest(duck->GetPosTry(), duck->GetBoundingSphereRadius(), m_xMin, m_yMin, m_xMax, m_yMax);
			if(collidedwithWorld)
			{
				duck->Collided();
				float deflectAngle = 180.0f + static_cast<float>(rand() % 15);
				duck->Divert(deflectAngle);
				duck->BounceBack();
			}			
		}
	}
}
