#ifndef TEXTURE_QUAD_3D_H
#define TEXTURE_QUAD_3D_H

#include <vector>
#include <memory>

// Forward declaration
class  SceneLightData;
struct RenderStateTexturedQuad3D;

class TexturedQuad3D
{
	public:

	/*
		Assumes order of v1, v2, v3 and v4 is as bellow:
		----> X
		|
		|
		|
		Y

		V1     V2

		V4     V3
	*/

	TexturedQuad3D(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, glm::vec3 v4, float textureRepeat, GLuint texture);
	TexturedQuad3D(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3, glm::vec3 v4, float textureRepeat, GLuint texture, 
					std::vector<glm::vec2> &verticesUV, float alphaSetting);
	
	~TexturedQuad3D();

	static void		InitTexturedQuad3D();

	GLuint		Texture() const;
	void		SetTexture(GLuint texture);
	void		SetTextureCoords(std::vector<glm::vec2>		&verticesUV);

	void		Render(const RenderStateTexturedQuad3D& renderState, const glm::mat4 &projection, const glm::mat4 &view, const glm::vec3 &pos);	
	
	private:

	void			PrepareRender(const RenderStateTexturedQuad3D& renderState, const glm::mat4 &projection, const glm::mat4 &view, 
									const glm::vec3 &pos);
	void			CreateRectangleUVData();

	void			BuildVBOS();

	std::vector<glm::vec3>		m_vertices;
	std::vector<glm::vec3>		m_normals;
	std::vector<glm::vec2>		m_verticesUV;

	GLuint						m_texture;
	float						m_textureRepeat;
	
	bool						m_usingTransparentShader;
	float						m_alphaSetting;
	
	
	GLuint	m_verticesVBO;
	GLuint	m_normalVBO;
	GLuint	m_verticesUVVBO;

	// Shader data

	struct shaderDataSimpleLight
	{
		GLuint m_programID;
		GLuint m_lightPos_worldspaceID;
		GLuint m_viewMatrixID;
		GLuint m_modelViewProjectionID;
		GLuint m_modelMatrixID;
		GLuint m_modelViewMatrixID;
		GLuint m_normalMatrixID;
		GLuint m_lightColorID;
		GLuint m_userClipPlane0ID;
		GLuint m_textureID;
	};

	struct shaderDataTransparent
	{
		GLuint m_programID;
		GLuint m_modelViewProjectionID;
		GLuint m_modelMatrixID;
		GLuint m_textureID;
		GLuint m_alphaChannelID;
		GLuint m_userClipPlane0ID;
	};

	static shaderDataSimpleLight m_simpleLightShader;
	static shaderDataTransparent m_transparentShader;
	
};

#endif