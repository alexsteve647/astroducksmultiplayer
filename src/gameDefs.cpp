// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include "gameDefs.h"
#include "gameScene.h"

//----------------------------------------------------------------------
GameDefData::GameDefData()
{
    Reset();
}
//----------------------------------------------------------------------
void GameDefData::Reset()
{
    m_playerDepth = -10.0f;
    m_reflectionOffset =  (-10.0f - m_playerDepth) / 1.0f;
    m_waterLevel = m_playerDepth - 0.11f;

    m_speedIncreaseFlow = 0.0f;
    m_speedDecreaseFlow = 0.0f;
    m_speedConstantDecreaseFlow = 0.0f;

    m_timeStartIncreaseFlow = -1.0f;
    m_timeToIncreaseFlow = 0.0f;

    m_timeStartDecreaseFlow = -1.0f;
    m_timeToIDeceaseFlow = 0.0f;
}
//----------------------------------------------------------------------
GameDefData::~GameDefData()
{

}
//----------------------------------------------------------------------
float GameDefData::GetPlayerDepth() const
{
    return m_playerDepth;
}
//----------------------------------------------------------------------
float GameDefData::GetReflectionOffset() const
{
    return m_reflectionOffset;
}
//----------------------------------------------------------------------
float GameDefData::GetWaterLevel() const
{
    return m_waterLevel;
}
//----------------------------------------------------------------------
void GameDefData::Tick(float deltaTime, GameScene &gameScene)
{
    float changeWaterLevel = m_speedIncreaseFlow - m_speedDecreaseFlow - m_speedConstantDecreaseFlow;
    ChangeWaterLevel(deltaTime * changeWaterLevel);
    gameScene.RecalculateWaterMatrices();

    if(m_timeStartIncreaseFlow > 0.0f)
    {
        float timeDiff = glfwGetTime() - m_timeStartIncreaseFlow;
        if(timeDiff > m_timeToIncreaseFlow)
        {
            m_timeStartIncreaseFlow = -1.0f;
            m_timeToIncreaseFlow = 0.0f;
            m_speedIncreaseFlow = 0.0f;
        }
    }

    if(m_timeStartDecreaseFlow > 0.0f)
    {
        float timeDiff = glfwGetTime() - m_timeStartDecreaseFlow;
        if(timeDiff > m_timeToIDeceaseFlow)
        {
            m_timeStartDecreaseFlow = -1.0f;
            m_timeToIDeceaseFlow = 0.0f;
            m_speedDecreaseFlow = 0.0f;
        }
    }
}
//----------------------------------------------------------------------
void GameDefData::AddToTimeToIncreaseFlow(float amount)
{
    if(m_timeStartIncreaseFlow < 0.0f)
    {
        m_timeStartIncreaseFlow = glfwGetTime();
    }
    m_timeToIncreaseFlow += amount;

    m_timeStartDecreaseFlow = -1.0f;
    m_timeToIDeceaseFlow = 0.0f;

    m_speedIncreaseFlow = 0.2f;
    m_speedDecreaseFlow = 0.0f;
}
//----------------------------------------------------------------------
void GameDefData::AddToTimeToDecreaseFlow(float amount)
{
    if(m_timeStartDecreaseFlow < 0.0f)
    {
        m_timeStartDecreaseFlow = glfwGetTime();
    }
    m_timeToIDeceaseFlow += amount;

    m_timeStartIncreaseFlow = -1.0f;
    m_timeToIncreaseFlow = 0.0f;

    m_speedIncreaseFlow = 0.0f;
    m_speedDecreaseFlow = 0.35f;
}
//----------------------------------------------------------------------
void GameDefData::ChangeWaterLevel(float amount)
{
    if(m_playerDepth + amount <= -10.0f && m_playerDepth + amount >= -14.7f)
    {
        m_playerDepth += amount;
        m_reflectionOffset =  (-10.0f - m_playerDepth) / 1.0f;
        m_waterLevel = m_playerDepth - 0.11f;
    }
}
