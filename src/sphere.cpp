#include "sphere.h"


Sphere::Sphere()
{
	m_pos = glm::vec3(0.0f, 0.0f, 0.0f);
	m_radius = 0.0f;
}
//----------------------------------------------------------------------
Sphere::Sphere(glm::vec3 pos, float radius)
{
	m_pos = pos;
	m_radius = radius;
}
//----------------------------------------------------------------------
Sphere::~Sphere()
{

}
//----------------------------------------------------------------------
const glm::vec3 & Sphere::GetPos() const
{
	return m_pos;
}
//----------------------------------------------------------------------	
const float &  Sphere::GetRadius() const
{
	return m_radius;
}
