#ifndef RENDER_TO_TEXTURE_H
#define RENDER_TO_TEXTURE_H


bool GenerateFrameBufferAndTexture(int width, int height, GLuint &frameBuffer, GLuint &renderedTexture, GLuint &depthrenderbuffer);
void CleanUpFrameBufferAndAssociatedData(GLuint frameBuffer, GLuint renderedTexture, GLuint depthrenderbuffer);

#endif 