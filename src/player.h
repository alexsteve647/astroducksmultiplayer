#ifndef PLAYER_H
#define PLAYER_H

#include <sstream>
#include <vector>
#include <memory>


// Fwds
class Model3d;
class Model3dCookTorrance;
class Duck;
class ProjectileBall;
class GameDefData;
struct RenderStateModel3D;

class Player
{
	public:
	Player(bool isSecondPlayer, const GameDefData& gameDefData);
	~Player();

	void 	SetPos(glm::vec3 pos);
	void 	SetRot(glm::vec3 rot);
	void 	SetScale(float scale);
	void 	SetFacingRotationImmediate(float facingRotation);
	void 	SetMoveDir(glm::vec3 dir);		
	void	SetMoveSpeed(float speed);
	void	SetColor(glm::vec3 color);

	void	TakeDamage(int damage);
	void	IncreaseScore(int score);
	void	HitWorldBorder();
	void	FireBall();

	void	DecreaseScore(int decrease);
	void	ResetScore();
	void	ResetHealth();
	void	ResetHealth(int startHealth);

	int				GetScore() const;
	int				GetHealth() const;
	glm::vec3&		GetPos();
	glm::vec3&		GetPosTry();
	glm::vec3&		GetMoveDir();
	glm::vec3&		GetRot();
	float& 			GetMoveSpeed();
	float&			GetFacingRotation();
	float&			GetBoundingSphereRadius();
	std::vector<std::unique_ptr<ProjectileBall> > &GetProjectileBalls();

	void Tick(float deltaTime);
	void DestroyProjectile(int projectileCounter);

	void Render(const RenderStateModel3D& renderState, const glm::mat4 &projection, const glm::mat4 &view);	

	static void InitPlayer();	
	static void CollisionTick(std::vector<std::unique_ptr<Player> > &players, float deltaTime);	

	private:

	static void	LoadModel();

	void 	TryMove(float deltaTime);

	void	DoMove(float deltaTime, glm::vec3 &pos);
	void	DoUpdateWaterLevel(glm::vec3 &pos);
	void 	DoWobble();
	void	TickProjectileBalls(float deltaTime);
	
	glm::vec3 	m_moveDir;
	glm::vec3 	m_pos;
	glm::vec3 	m_posTry;
	glm::vec3 	m_rot;
	glm::vec3	m_scale;
	glm::vec3 	m_color;
	float		m_moveSpeed;
	float		m_moveSpeedOrg;
	float		m_boundingSphereRadius;

	float		m_wobbleStartTime;
	float		m_wobbleCycleTime;
	float		m_wobbleAmount;

	double		m_playerInvisibleStartTime; // If player takes damage, make player invinsible for a short time
	bool		m_playerInivisible;

	std::vector<std::unique_ptr<ProjectileBall> > m_projectileBalls;

	int		m_health;
	int		m_score;
	int		m_scoreNewLife;
	bool	m_hitWorldBorder;

	bool	m_isSecondPlayer;
	
	static Model3d* m_model;
	static Model3dCookTorrance* m_modelCookTorrance;

	const GameDefData&			m_gameDefData;
};

#endif