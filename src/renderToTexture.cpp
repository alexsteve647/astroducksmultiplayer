#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

using namespace std;

#include "renderToTexture.h"

bool GenerateFrameBufferAndTexture(int width, int height, GLuint &frameBuffer, GLuint &renderedTexture, GLuint &depthrenderbuffer)
{
	frameBuffer = 0;
	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	renderedTexture = 0;
	glGenTextures(1, &renderedTexture);

	glBindTexture(GL_TEXTURE_2D, renderedTexture);

	glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, width, height, 0,GL_RGB, GL_UNSIGNED_BYTE, 0);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);	

	// We want a depth buffer too
	depthrenderbuffer = 0;
	glGenRenderbuffers(1, &depthrenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);	

	// Set "renderedTexture" as our colour attachement #0
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, renderedTexture, 0);	

	bool retValue = true;
	// Check that our framebuffer is ok
	GLenum frameBuffStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);

	if(frameBuffStatus != GL_FRAMEBUFFER_COMPLETE)
	{
		retValue = false;	
	}


	// Default lets render to the screen
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return retValue;	
}
//----------------------------------------------------------------------
void CleanUpFrameBufferAndAssociatedData(GLuint frameBuffer, GLuint renderedTexture, GLuint depthrenderbuffer)
{
	glDeleteFramebuffers(1, &frameBuffer);
	glDeleteTextures(1, &renderedTexture);
	glDeleteRenderbuffers(1, &depthrenderbuffer);
}