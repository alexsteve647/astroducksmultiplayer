#include <iostream>
#include <string.h>
using namespace std;
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "initGL.h"
#include "gui.h"
#include "texture.h"
#include "texturedQuad.h"
#include "text2D.h"
#include "inGameHUD.h"

//#define DEBUG

InGameHUD::InGameHUD(bool singlePlayer)
{
	m_gameHUD = new HUDGUI;

	m_playerHealth = 5;
	m_playerScore = 0;

	m_playerTwoHealth = singlePlayer ? 0 : 5;
	m_playerTwoScore = 0;

	m_timeLeftMinutes = 0;
	m_timeLeftSeconds = 0;
	

	for(int i = 0; i < m_playerHealth; i++)
	{
		float size = 50.0f;
		float offsetX = (size / 2.0f) + 5.0f;
		float xPos = static_cast<float>(i) * (50.0f + 5.0f) + offsetX;
		TexturedQuad* health = new TexturedQuad(glm::vec3(xPos, 80.0f, 0.0f), size, size, GetTextureResource(HUD_PLAYER_HEART_FULL, false));
		m_HUDElements.push_back(health);
	}

	if(!singlePlayer)
	{
		for(int i = 0; i < m_playerTwoHealth; i++)
		{
			float size = 50.0f;
			float offsetX = (size / 2.0f) + 5.0f;
			float xPos = static_cast<float>(i) * (50.0f + 5.0f) + offsetX;
			TexturedQuad* health = new TexturedQuad(glm::vec3(xPos, 135.0f, 0.0f), size, size, GetTextureResource(HUD_PLAYER_HEART_FULL, false));
			m_HUDElementsPlayerTwo.push_back(health);
		}
	}

	const float playerScoreTextXPos = s_windowWidth - 270.0f;
	const float playerScoreTextYPos = 80.0f;
	m_playerScoreText = new Text2D("0000000", glm::vec3(playerScoreTextXPos, playerScoreTextYPos, 0.0f));

	const float player2ScoreTextYPos = playerScoreTextYPos + 30.0f;
	m_playerTwoScoreText = nullptr;
	if(!singlePlayer)
	{
		
		m_playerTwoScoreText = new Text2D("0000000", glm::vec3(playerScoreTextXPos, 110.0f, 0.0f));
	}

	const float timeMainLoopTimeYPos = singlePlayer ? playerScoreTextYPos + 30.0f : player2ScoreTextYPos + 30.0f;
	m_timeMainLoopTime = new Text2D("0,00000", glm::vec3(playerScoreTextXPos, timeMainLoopTimeYPos, 0.0f));

	const float textXStartPos = static_cast<float>(s_windowWidth) / 2.0f - 87.0f;
	const float textColonSeparatorPos = textXStartPos + 65.0f;
	const float timeLeftYPos = 80.0f;
	const float minutesAndSecYPos = timeLeftYPos + 40.0f;
	

	m_TimeLeftLabel = new Text2D("TIME LEFT", glm::vec3(textXStartPos - 87.0f, timeLeftYPos, 0.0f));
	m_timeLeftMinutesText = new Text2D("00", glm::vec3(textXStartPos, minutesAndSecYPos, 0.0f));
	m_timeLeftSecondsText = new Text2D("00", glm::vec3(textXStartPos + 100.0f, minutesAndSecYPos, 0.0f));
	m_timeLeftColonSeparator = new TexturedQuad(glm::vec3(textColonSeparatorPos, minutesAndSecYPos, 0.0f), 35.0f, 35.0f, 
												GetTextureResource(HUD_TEXT2D_COLON, false));		

	const float gameOverXPos = textColonSeparatorPos - 100.0f; 
	const float gameOverYPos = static_cast<float>(s_windowHeight) / 2.0f; 
	m_gameOverSign = new Text2D("GAME OVER", glm::vec3(gameOverXPos, gameOverYPos, 0.0f));

	const float restartGameXPos = textColonSeparatorPos - 325.0f; 
	const float restartGameYPos = gameOverYPos + 40.0f; 
	m_restartGameSign = new Text2D("PRESS ENTER TO RESTART", glm::vec3(restartGameXPos, restartGameYPos, 0.0f));


	m_showGameOverSign = false;
	m_showTimeMainLoopTime = false;

	m_debugTexturedQuad = nullptr;
}
//----------------------------------------------------------------------
InGameHUD::~InGameHUD()
{
	delete m_gameHUD;
	delete m_playerScoreText;

	if(m_playerTwoScoreText)
	{
		delete m_playerTwoScoreText;
	}

	for(auto &HUDHealth : m_HUDElements)
	{
		delete HUDHealth;
	}

	for(auto &HUDHealth : m_HUDElementsPlayerTwo)
	{
		delete HUDHealth;
	}

	delete m_timeLeftColonSeparator;
	delete m_gameOverSign;
	delete m_restartGameSign;
	delete m_TimeLeftLabel;
	delete m_timeLeftSecondsText;
}
//----------------------------------------------------------------------
void InGameHUD::UpdatePlayerHealth(int playerHealth)
{
	m_playerHealth = playerHealth;
}
//----------------------------------------------------------------------
void InGameHUD::UpdatePlayerScore(int playerScore)
{
	m_playerScore = playerScore;

	if(m_playerScore <= 9999999)
	{
		UpdateText(m_playerScoreText, m_playerScore, 8);
	}
}	
//----------------------------------------------------------------------
void InGameHUD::UpdatePlayerTwoHealth(int playerHealth)
{
	m_playerTwoHealth = playerHealth;
}
//----------------------------------------------------------------------
void InGameHUD::UpdatePlayerTwoScore(int playerScore)
{
	m_playerTwoScore = playerScore;

	if(m_playerScore <= 9999999)
	{
		UpdateText(m_playerTwoScoreText, m_playerTwoScore, 8);
	}
}
//----------------------------------------------------------------------
void InGameHUD::UpdateText(Text2D* text, int value, int length)
{
	/*
		Will output the value in reverse to buff, then we simply flip it
	*/
	char buff[length];
	memset(buff, '0', length - 1);
	buff[length - 1] = '\0';
	int tmp = value;
	int i = 0;
	while(tmp > 0 && i < length)
	{
		char c = tmp % 10 + '0';
		buff[i] = c;
		i++;
		tmp = tmp / 10;
	}

	char buff_reversed[length];
	memset(buff_reversed, '0', length - 1);
	i = 0;
	while(i < length - 1)
	{
		buff_reversed[(length - 1) - 1 - i] = buff[i];
		i++;
	}
	buff_reversed[length - 1] = '\0';
	text->UpdateText(buff_reversed);
}
//----------------------------------------------------------------------
void InGameHUD::UpdateTimeLeft(int minutes, int seconds)
{
	m_timeLeftMinutes = minutes;
	m_timeLeftSeconds = seconds;

	UpdateText(m_timeLeftMinutesText, m_timeLeftMinutes, 3);
	UpdateText(m_timeLeftSecondsText, m_timeLeftSeconds, 3);
}
//----------------------------------------------------------------------
void InGameHUD::SetMainLoopTime(float v)
{
	char buffer [8];
	memset(buffer, '\0', strlen(buffer) + 1);
	if(v > 1 && v < 10)
	{
		sprintf(buffer, "%.0f", v);
	}
	else
	{
		sprintf(buffer, "%.5f", v);
	}
	m_timeMainLoopTime->UpdateText(buffer);
}
//----------------------------------------------------------------------
void InGameHUD::PrepareHUDGUIForRender()
{
	m_gameHUD->Clear();

	if(m_showGameOverSign)
	{
		m_gameHUD->AddText(m_gameOverSign);
		m_gameHUD->AddText(m_restartGameSign);
	}

	if(m_showTimeMainLoopTime)
	{
		m_gameHUD->AddText(m_timeMainLoopTime);
	}

	for(int i = 0; i < m_playerHealth; i++)
	{
		m_gameHUD->AddTexturedQuad(m_HUDElements[i]);
	}

	for(int i = 0; i < m_playerTwoHealth; i++)
	{
		m_gameHUD->AddTexturedQuad(m_HUDElementsPlayerTwo[i]);
	}

	m_gameHUD->AddTexturedQuad(m_timeLeftColonSeparator);

	m_gameHUD->AddText(m_playerScoreText);

	if(m_playerTwoScoreText)
	{
		m_gameHUD->AddText(m_playerTwoScoreText);
	}
	
	m_gameHUD->AddText(m_TimeLeftLabel);
	m_gameHUD->AddText(m_timeLeftMinutesText);
	m_gameHUD->AddText(m_timeLeftSecondsText);

	if(m_debugTexturedQuad)
	{
		m_gameHUD->AddTexturedQuad(m_debugTexturedQuad);
	}
}
//----------------------------------------------------------------------
void InGameHUD::ShowGameOverSign(bool b)
{
	m_showGameOverSign = b;
}
//----------------------------------------------------------------------
void InGameHUD::ShowTimeMainLoopTime(bool b)
{
	m_showTimeMainLoopTime = b;
}
//----------------------------------------------------------------------
void InGameHUD::DebugAddTextureQad(TexturedQuad* theQuad)
{
	m_debugTexturedQuad = theQuad;
}
//----------------------------------------------------------------------
HUDGUI* InGameHUD::GetGameHud() const
{
	return m_gameHUD;
}