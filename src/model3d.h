#ifndef MODEL3D_H
#define MODEL3D_H


// Fwds
struct RenderStateModel3D;

class Model3d
{
	public:
	Model3d();
	~Model3d();

	void	Build(const char *modelPath);
	void	SetTexture(GLuint texture);
	void	Render(const RenderStateModel3D& renderState, const glm::mat4 &projection, 
					const glm::mat4 &view, const glm::vec3 &pos, const glm::vec3 &rot, const glm::vec3 &scale);

	static void InitModel3d();
	static void PrepareRender();
	static void AfterRender();

	private:

	// BOs containing vertex and normal data for the model
	GLuint	m_vertexBuffer;
	GLuint	m_uvCoords;
	GLuint	m_normalBuffer;

	// Texture of the model
	GLuint		m_texture;

	unsigned int	m_vertexBufferSize;

	// Shader handles
	struct shaderProgramHandles
	{
		GLuint m_programID;
		GLuint m_lightPos_worldspaceID;
		GLuint m_viewMatrixID;
		GLuint m_modelViewProjectionID;
		GLuint m_modelMatrixID;
		GLuint m_modelViewMatrixID;
		GLuint m_normalMatrixID;
		GLuint m_userClipPlane0ID;

		GLuint m_lightColorID;

		GLuint m_textureID;
	};

	static shaderProgramHandles m_model3DShaderHandles;
	
	// If true, BOs have been built
	bool			m_built;
};

#endif