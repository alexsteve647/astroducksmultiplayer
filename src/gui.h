#ifndef GUI_H
#define GUI_H

#include <vector>

// Forward declarations
class TexturedQuad;
class Text2D;

class HUDGUI
{
	public:
	HUDGUI();
	~HUDGUI();

	void AddTexturedQuad(TexturedQuad* textureQuad);
	void AddText(Text2D* text);
	void Clear();
	void Tick();	

	void Render(const glm::mat4 &projection, const glm::mat4 &view);
	private:

	std::vector<Text2D*>		m_texts;
	std::vector<TexturedQuad*> 	m_textureQuads;

};

#endif