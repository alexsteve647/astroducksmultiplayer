#include <iostream>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

using namespace glm;
using namespace std;

#include "renderToTexture.h"
#include "gameScene.h"
#include "waterGfxEffect.h"
#include "texturedQuad3D.h"
#include "player.h"
#include "initGL.h"
#include "gameDefs.h"

const int s_reflectionTextureWidth = 1024;
const int s_reflectionTextureHeight = 1024;


WaterGfxEffect::WaterGfxEffect(const GameDefData&  gameDefData) : m_gameDefData(gameDefData)
{
	m_pos = glm::vec3(0.0f, 0.0f, m_gameDefData.GetWaterLevel());

	// Frame buffer for rendering reflection of water
	if(!GenerateFrameBufferAndTexture(s_reflectionTextureWidth, s_reflectionTextureHeight, m_reflectionFrameBuffer, m_reflectionRenderTexture, m_reflectionDepthBuffer))
	{
		cout << "Unable to create frame buffer data for reflections" << endl;
	}

	std::vector<glm::vec2> verticesUV;
	glm::vec2 UV1(0.0f, 0.0f);
	glm::vec2 UV2(1.0f, 0.0f);
	glm::vec2 UV3(1.0f, 1.0f);
	verticesUV.push_back(UV1);
	verticesUV.push_back(UV2);
	verticesUV.push_back(UV3);

	glm::vec2 UV4(1.0f, 1.0f);
	glm::vec2 UV5(0.0f, 1.0f);
	glm::vec2 UV6(0.0f, 0.0f);
	verticesUV.push_back(UV4);
	verticesUV.push_back(UV5);
	verticesUV.push_back(UV6);

	m_waterSurface = new TexturedQuad3D(glm::vec3(WORLD_XMIN, WORLD_YMIN, 0.0f), 
										glm::vec3(WORLD_XMAX, WORLD_YMIN, 0.0f), 
										glm::vec3(WORLD_XMAX, WORLD_YMAX, 0.0f), 
										glm::vec3(WORLD_XMIN, WORLD_YMAX, 0.0f), 
										1.0f, 
										m_reflectionRenderTexture,
										verticesUV,
										0.5f);
}
//----------------------------------------------------------------------
void WaterGfxEffect::CreateReflectionTexture(const GameScene* gameScene)
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_reflectionFrameBuffer);

	// m_backgroundColor = glm::vec3(0.0f, 0.41f, 0.66f);
	glClearColor(0.0f, 0.41f, 0.66f, 0.5f);
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glViewport(0, 0, s_reflectionTextureWidth, s_reflectionTextureHeight);
	glEnable(GL_CLIP_DISTANCE0);

	// If we cull the face, things will get distorted when create the reflection texture.
	// We might be able to just flip from counter-clockwise to clockwise order fiddle
	// with some other paramter to make it work here too.
	glDisable(GL_CULL_FACE);
	gameScene->RenderSceneWithoutHUD(gameScene->WaterViewMatrix(), gameScene->WaterProjectionMatrix(), false, false, true);

	// Reset - turn of clip plane, reset frame buffer to the screen and reset the viewport
	glEnable(GL_CULL_FACE); 
	glDisable(GL_CLIP_DISTANCE0);	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, s_windowWidth, s_windowHeight);
}
//----------------------------------------------------------------------
void WaterGfxEffect::Tick()
{
	m_pos.z = m_gameDefData.GetWaterLevel();
}
//----------------------------------------------------------------------
void WaterGfxEffect::Render(const RenderStateTexturedQuad3D& renderState, const glm::mat4 &projection, const glm::mat4 &view)
{
	m_waterSurface->Render(renderState, projection, view, m_pos);
}
//----------------------------------------------------------------------
WaterGfxEffect::~WaterGfxEffect()
{
	CleanUpFrameBufferAndAssociatedData(m_reflectionFrameBuffer, m_reflectionRenderTexture, m_reflectionDepthBuffer);

	delete m_waterSurface;
}