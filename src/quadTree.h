#ifndef QUADTREE_H
#define QUADTREE_H

#include <vector>
#include <memory>
#include <glm/glm.hpp>

//Fwds
class Duck;

/* I've choosen a special dedicated quadtree for ducks, because the heavy part of the collision will be duck vs duck.
In order to avoid virtual functions or copying of data (position & radius) overhead, this design was choosen.
*/

/* 2D quad tree for ducks

    Q4 | Q3
    ---------
    Q1 | Q2

*/    
class QuadtreeDucks
{
    public:
    static unsigned int s_quadTreeMaxOjbs;
    static unsigned int s_quadTreeMaxLevels;

    QuadtreeDucks(int level);
    ~QuadtreeDucks();

    void SetWorldSize(float xMin, float yMin, float xMax, float yMax);
    void Clear();
    void Insert(Duck* duck);
    void GetCollidables(Duck* duck, std::vector<Duck* > &ducks);

    void PrintTree();

    // Interface for test-cases
    bool VerifyNrDucks(int expectedNrDucks) const;
    bool VerifyTreeDepth(int expectedDepth) const;


    private:

    int GetNrDucks() const;
    int GetTreeDepth(int &level) const;

    int GetIndex(glm::vec3 &duckPos, float boundingSphereRadius);
    void GetChildren(std::vector<Duck* > &ducks);
    void Split();

    void DebugPrintIndent();
    
    int m_level;
    float m_xMin, m_xMax;
    float m_yMin, m_yMax;

    std::vector<QuadtreeDucks*> m_children;
    std::vector<Duck* > m_ducks;
};

#endif