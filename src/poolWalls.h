#ifndef POOL_WALLS_H
#define POOL_WALLS_H


// Fwds
class TexturedQuad3D;
struct RenderStateTexturedQuad3D;

class PoolWalls
{
    public:
    PoolWalls();
    ~PoolWalls();

    void Render(const RenderStateTexturedQuad3D& renderState, const glm::mat4 &projection, const glm::mat4 &view);

    private:

    std::vector<TexturedQuad3D* > m_poolWalls;
};

#endif 