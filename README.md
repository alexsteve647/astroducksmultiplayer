AstroDucks Multiplayer TCP/IP
========================

This is is a work in progress!

```bash
To build server:
make dir build
step into build
cmake ..
make astroDucks

To build client:
Same steps as for server then run:
make astroDucksClient

To run server:
unzip textures.zip in the folder it is located in.
run astroDucks
The server will wait for a client to connect, once that happens you can press enter to start the server. The ducks will not move in this
version as it still under development - only the boat can be moved using the cursor keys.

To run client:
unzip textures.zip in the folder it is located in.
run astroDucksClient
The client will try to connect to a server on 127.0.0.1 and then wait for you to press enter to star the client game session.
The ducks wont move in this version as it is still in progress, the client boat can be moved with the W, A, D keys. The other boats
position and rotation is received from the server.

```

